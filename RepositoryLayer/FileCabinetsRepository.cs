﻿using Interfaces.Data;
using Interfaces.Repositories;
using Models.DbEntities;
using Models.Dtos;
using Ninject;
using System;
using System.Collections.Generic;

namespace RepositoryLayer
{
    public class FileCabinetsRepository : DbRepository, IFileCabinetsRepository
    {
        public FileCabinetsRepository(IDataFacade dataFacade, IDataQuery dq, string tableName)
            : base(dataFacade, dq, tableName)  { }

        public IList<FileCabinet> GetFileCabinets(SearchQuery searchQuery)
        {
            _dq.Init(_tableName);

            if(searchQuery == null)
            {
                
                return _dataFacade.GetTableData<FileCabinet>(_dq);
            }

            _dq
              .AddQuery("Deleted", 0)
              .SearchBy("Name", searchQuery.SearchValue)
              .GetPage(searchQuery.PageNumber, searchQuery.ElementsPerPage)
              .SortBy(searchQuery.SortProperty, searchQuery.IsAscending);

            return _dataFacade.GetTableData<FileCabinet>(_dq);
        }


        public FileCabinet GetFileCabinetById(Guid guid)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", guid);

            var fcs = _dataFacade.GetTableData<FileCabinet>(_dq);
            if(fcs.Count == 0)
            {
                return null;
            }
            return fcs[0];
        }

        public bool AddFileCabinet(FileCabinet fileCabinet)
        {
            _dq.Init(_tableName);
            return _dataFacade.AddObject<FileCabinet>(fileCabinet, _dq);
        }

        public FileCabinet UpdateFileCabinet(FileCabinet updatedFileCabinet)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", updatedFileCabinet.Guid.ToString());

            _dataFacade.ModifyObject<FileCabinet>(updatedFileCabinet, _dq);

            return updatedFileCabinet;
        }

        public int GetCount(SearchQuery searchQuery)
        {
            _dq.Init("FileCabinets");

            if (searchQuery == null)
            {
                return _dataFacade.GetTableRowCount(_dq);
            }

            _dq
                .AddQuery("Deleted", 0)
                .SearchBy("Name", searchQuery.SearchValue);

            return _dataFacade.GetTableRowCount(_dq);
        }
    }
}