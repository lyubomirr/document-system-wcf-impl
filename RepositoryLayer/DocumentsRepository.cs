﻿using Interfaces.Repositories;
using Interfaces.Data;
using System.Collections.Generic;
using System;
using Models.DbEntities;
using Models.Dtos;

namespace RepositoryLayer
{
    public class DocumentsRepository : DbRepository, IDocumentsRepository
    {
        private string[] _searchProperties = { "Name", "Description", "Subject", "Contact", "Company", "RegDocNumber" };     

        public DocumentsRepository(IDataFacade dataFacade,IDataQuery dq, string tableName) 
            : base(dataFacade, dq, tableName) {  }    

        public Document GetDocumentById(Guid guid)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", guid)
                .Join("Users", "OwnerId", "Guid");

            var docs = _dataFacade.GetJoinedTableData<Document, User>(_dq, "Owner");

            if (docs.Count == 0)
            {
                return null;
            }
            return docs[0];
        }

        public bool AddDocument(Document doc)
        {
            _dq.Init(_tableName);
            return _dataFacade.AddObject<Document>(doc, _dq);
        }

        public Document UpdateDocument(Document updatedDoc)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", updatedDoc.Guid);

            _dataFacade.ModifyObject<Document>(updatedDoc, _dq);
            return updatedDoc;
        }

        public IList<Document> GetDocuments(Guid? fcGuid = null, SearchQuery searchQuery = null)
        {
            _dq.Init(_tableName)
                .Join("Users", "OwnerId", "Guid")
                .AddQuery("FileCabinetId", fcGuid)
                .AddQuery("Deleted", 0)
                .AddQuery("Archived", 0);

            if (searchQuery != null)
            {
                _dq
                    .AddQuery("TypeId", searchQuery.Type)
                    .SearchBy(_searchProperties, searchQuery.SearchValue)
                    .SortBy(searchQuery.SortProperty, searchQuery.IsAscending)
                    .GetPage(searchQuery.PageNumber, searchQuery.ElementsPerPage);
            }

            return _dataFacade.GetJoinedTableData<Document, User>(_dq, "Owner");
        }

        public IList<Document> GetInactiveDocuments(InactiveState state, SearchQuery searchQuery = null, Guid? fcGuid = null)
        {
            _dq.Init(_tableName)
                .Join("Users", "OwnerId", "Guid")
                .AddQuery("FileCabinetId", fcGuid);

            if (searchQuery != null)
            {
                _dq
                    .AddQuery("TypeId", searchQuery.Type)
                    .SearchBy(_searchProperties, searchQuery.SearchValue)
                    .GetPage(searchQuery.PageNumber, searchQuery.ElementsPerPage)
                    .SortBy(searchQuery.SortProperty, searchQuery.IsAscending);
            }
                
            switch(state)
            {
                case InactiveState.Deleted:
                    {
                        _dq.AddQuery("Deleted", 1);
                        break;
                    }
                case InactiveState.Archived:
                    {
                        _dq.AddQuery("Archived", 1);
                        break;
                    }
            }

            return _dataFacade.GetJoinedTableData<Document, User>(_dq, "Owner");
        }

        public int GetDocumentCount(Guid? fcGuid = null, SearchQuery searchQuery = null)
        {
            _dq.Init(_tableName)
                .AddQuery("FileCabinetId", fcGuid)
                .AddQuery("Deleted", 0)
                .AddQuery("Archived", 0);
            
            if(searchQuery != null)
            {
                _dq
                    .AddQuery("TypeId", searchQuery.Type)
                    .SearchBy(_searchProperties, searchQuery.SearchValue);
            }

            return _dataFacade.GetTableRowCount(_dq);
        }

        public int GetInactiveDocumentCount(InactiveState state, SearchQuery searchQuery = null)
        {
            _dq.Init(_tableName);

            if(searchQuery != null)
            {
                _dq
                    .AddQuery("TypeId", searchQuery.Type)
                    .SearchBy(_searchProperties, searchQuery.SearchValue);
            }

            switch (state)
            {
                case InactiveState.Deleted:
                    {
                        _dq.AddQuery("Deleted", 1);
                        break;
                    }
                case InactiveState.Archived:
                    {
                        _dq.AddQuery("Archived", 1);
                        break;
                    }
            }

            return _dataFacade.GetTableRowCount(_dq);
        }
    }
}