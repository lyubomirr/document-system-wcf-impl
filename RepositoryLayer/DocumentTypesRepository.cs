﻿using Interfaces.Data;
using Interfaces.Repositories;
using Models.DbEntities;
using Ninject;
using System.Collections.Generic;

namespace RepositoryLayer
{
    public class DocumentTypesRepository : DbRepository, IDocumentTypesRepository
    {
        public DocumentTypesRepository(IDataFacade dataFacade, IDataQuery dq, string tableName)
            : base(dataFacade, dq, tableName) { }

        public IList<DocumentType> GetDocumentTypes()
        {
            _dq.Init(_tableName);
            return _dataFacade.GetTableData<DocumentType>(_dq);
        }
    }
}