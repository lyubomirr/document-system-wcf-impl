﻿using Interfaces.Data;
using Interfaces.Repositories;
using Models.DbEntities;
using Ninject;
using System;
using System.Collections.Generic;

namespace RepositoryLayer
{
    public class UsersRepository : DbRepository, IUsersRepository
    {
        public UsersRepository(IDataFacade dataFacade, IDataQuery dq, string tableName)
            : base(dataFacade, dq, tableName) { }

        public IList<User> GetUsers(string username = null)
        {
            _dq.Init(_tableName)
                .AddQuery("Username", username);

            return _dataFacade.GetTableData<User>(_dq);
        }

        public User GetUserById(Guid guid)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", guid);

            var users = _dataFacade.GetTableData<User>(_dq);

            if(users.Count == 0)
            {
                return null;
            }

            return users[0];
        }
    }
}