﻿using Interfaces.Data;
using Interfaces.Repositories;
using Models.DbEntities;
using Ninject;
using System;
using System.Collections.Generic;

namespace RepositoryLayer
{
    public class RolesRepository : DbRepository, IRolesRepository
    {
        public RolesRepository(IDataFacade dataFacade, IDataQuery dq, string tableName)
            : base(dataFacade, dq, tableName) { }

        public Role GetRoleById(Guid guid)
        {
            _dq.Init(_tableName)
                .AddQuery("Guid", guid);

            var roles = _dataFacade.GetTableData<Role>(_dq);
            if(roles.Count == 0)
            {
                return null;
            }

            return roles[0];
        }

        public IList<Role> GetRoles()
        {
            _dq.Init(_tableName);
            return _dataFacade.GetTableData<Role>(_dq);
        }
    }
}