﻿using Configuration;
using Interfaces.Data;
using Interfaces.Repositories;
using Models.DbEntities;
using Moq;
using NUnit.Framework;
using RepositoryLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace UnitTests.RepositoryLayer
{
    [TestFixture]
    public class DocumentsRepositoryTests
    {
        private Mock<IDataQuery> dqMock = GetDqMock();

        [Test]
        public void GetDocumentById_ShouldComplete()
        {
            var dataFacadeMock = new Mock<IDataFacade>();
            dataFacadeMock.Setup(x => x.GetJoinedTableData<Document, User>(dqMock.Object, It.IsAny<string>()))
                .Returns(new List<Document>() { GetDocumentsData()[0] });

            var repo = new DocumentsRepository(dataFacadeMock.Object, dqMock.Object, It.IsAny<string>());

            string expected;
            string actual;

            var serializer = new XmlSerializer(typeof(Document));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, GetDocumentsData()[0]);
                expected = writer.ToString();
                writer.GetStringBuilder().Clear();

                serializer.Serialize(writer, repo.GetDocumentById(GetDocumentsData()[0].Guid));
                actual = writer.ToString();
            }

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void GetDocumentById_ShouldReturnNullIfNoDocument()
        {
            var dataFacadeMock = new Mock<IDataFacade>();
            dataFacadeMock.Setup(x => x.GetJoinedTableData<Document, User>(dqMock.Object, It.IsAny<string>()))
                .Returns(new List<Document>());

            var repo = new DocumentsRepository(dataFacadeMock.Object, dqMock.Object, It.IsAny<string>());

            Assert.IsNull(repo.GetDocumentById(GetDocumentsData()[0].Guid));
        }
        [Test]
        public void GetDocuments_ShouldComplete()
        {
            var dataFacadeMock = new Mock<IDataFacade>();
            dataFacadeMock.Setup(x => x.GetJoinedTableData<Document, User>(dqMock.Object, It.IsAny<string>()))
                .Returns(GetDocumentsData());

            var repo = new DocumentsRepository(dataFacadeMock.Object, dqMock.Object, It.IsAny<string>());

            string expected;
            string actual;

            var serializer = new XmlSerializer(typeof(List<Document>));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, GetDocumentsData());
                expected = writer.ToString();
                writer.GetStringBuilder().Clear();

                serializer.Serialize(writer, repo.GetDocuments());
                actual = writer.ToString();
            }

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void GetInactiveDocuments_ShouldComplete()
        {
            var dataFacadeMock = new Mock<IDataFacade>();
            dataFacadeMock.Setup(x => x.GetJoinedTableData<Document, User>(dqMock.Object, It.IsAny<string>()))
                .Returns(GetDocumentsData());

            var repo = new DocumentsRepository(dataFacadeMock.Object, dqMock.Object, It.IsAny<string>());

            string expected;
            string actual;

            var serializer = new XmlSerializer(typeof(List<Document>));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, GetDocumentsData());
                expected = writer.ToString();
                writer.GetStringBuilder().Clear();

                serializer.Serialize(writer, repo.GetInactiveDocuments(It.IsAny<InactiveState>()));
                actual = writer.ToString();
            }

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void GetDocumentsCount_ShouldComplete()
        {          
            var dataFacadeMock = new Mock<IDataFacade>();

            dataFacadeMock.Setup(x => x.GetTableRowCount(dqMock.Object)).Returns(GetDocumentsData().Count);

            var repo = new DocumentsRepository(dataFacadeMock.Object, dqMock.Object, It.IsAny<string>());

            var expected = GetDocumentsData().Count;
            var actual = repo.GetDocumentCount();

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void GetInactiveDocumentsCount_ShouldComplete()
        {
            var dataFacadeMock = new Mock<IDataFacade>();

            dataFacadeMock.Setup(x => x.GetTableRowCount(dqMock.Object)).Returns(GetDocumentsData().Count);

            var repo = new DocumentsRepository(dataFacadeMock.Object, dqMock.Object, It.IsAny<string>());

            var expected = GetDocumentsData().Count;
            var actual = repo.GetInactiveDocumentCount(It.IsAny<InactiveState>());

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void UpdateDocument_ShouldComplete()
        {
            var dataFacadeMock = new Mock<IDataFacade>();
            dataFacadeMock.Setup(x => x.ModifyObject<Document>(GetDocumentsData()[0], dqMock.Object));

            var repo = new DocumentsRepository(dataFacadeMock.Object, dqMock.Object, It.IsAny<string>());

            string expected;
            string actual;

            var serializer = new XmlSerializer(typeof(Document));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, GetDocumentsData()[0]);
                expected = writer.ToString();
                writer.GetStringBuilder().Clear();

                serializer.Serialize(writer, repo.UpdateDocument(GetDocumentsData()[0]));
                actual = writer.ToString();
            }

            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void AddDocument_ShouldComplete()
        {
            var document = GetDocumentsData()[0];
            var dataFacadeMock = new Mock<IDataFacade>();

            dataFacadeMock.Setup(x => x.AddObject<Document>(document, dqMock.Object)).Returns(true);

            var repo = new DocumentsRepository(dataFacadeMock.Object, dqMock.Object, It.IsAny<string>());
            Assert.IsTrue(repo.AddDocument(document));
        }

        private List<Document> GetDocumentsData()
        {
            var first = new Document
            {
                Guid = Guid.Parse("c35aac71-41d8-4450-a4a8-c928075e447c"),
                Name = "First Document",
                Description = "Some doc",
                TypeId = Guid.Parse("9cc8ae04-7a69-498d-bbf3-68cdd85a4356")
            };
            var second = new Document
            {
                Guid = Guid.Parse("8b0b83f8-c38f-467a-8907-7b9598c5376a"),
                Name = "Second Document",
                Description = "Another doc",
                TypeId = Guid.Parse("1f6e7627-8b82-4b60-a0c9-4615a6579233")
            };
            var third = new Document
            {
                Guid = Guid.Parse("e68b9fc7-f9dd-4c3a-b579-4aea469b80cf"),
                Name = "Third Document",
                Description = "some other doc",
                TypeId = Guid.Parse("1a13227c-289f-4524-ba83-ef07e90b4c6d")
            };

            return new List<Document> { first, second, third };
        }
        private TableMapping GetDocumentMapping()
        {
            var pairs = new List<Pair>
            {
                new Pair
                {
                    DataSource = "id",
                    Model = "Guid"
                },
                new Pair
                {
                    DataSource = "name",
                    Model = "Name"
                },
                new Pair
                {
                    DataSource = "description",
                    Model = "Description"
                },
                new Pair
                {
                    DataSource = "type_id",
                    Model = "TypeId"
                },
                new Pair
                {
                    DataSource = "archived",
                    Model = "Archived"
                },
                new Pair
                {
                    DataSource = "deleted",
                    Model = "Deleted"
                }
            };
            var mapping = new TableMapping();
            mapping.Pairs = pairs;
            mapping.TableName = "Documents";
            return mapping;
        }
        private static Mock<IDataQuery> GetDqMock()
        {
            var dqMock = new Mock<IDataQuery>();
            dqMock.Setup(x => x.Init(It.IsAny<string>())).Returns(dqMock.Object);
            dqMock.Setup(x => x.AddQuery(It.IsAny<string>(), It.IsAny<Guid?>())).Returns(dqMock.Object);
            dqMock.Setup(x => x.AddQuery(It.IsAny<string>(), It.IsAny<object>())).Returns(dqMock.Object);
            dqMock.Setup(x => x.AddQuery(It.IsAny<string>(), It.IsAny<string>())).Returns(dqMock.Object);
            dqMock.Setup(x => x.Join(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(dqMock.Object);
            dqMock.Setup(x => x.SearchBy(It.IsAny<string>(), It.IsAny<string>())).Returns(dqMock.Object);
            dqMock.Setup(x => x.SearchBy(It.IsAny<string[]>(), It.IsAny<string>())).Returns(dqMock.Object);
            dqMock.Setup(x => x.SortBy(It.IsAny<string>(), It.IsAny<bool?>())).Returns(dqMock.Object);
            dqMock.Setup(x => x.GetPage(It.IsAny<int?>(), It.IsAny<int?>())).Returns(dqMock.Object);

            return dqMock;
        }
    }
}
