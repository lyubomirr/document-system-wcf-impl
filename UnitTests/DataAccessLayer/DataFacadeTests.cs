﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Configuration;
using DataAccessLayer;
using Interfaces.Data;
using Moq;
using NUnit.Framework;

namespace UnitTests.DataAccessLayer
{
    public class School
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class Person
    {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public int SchoolId { get; set; }
        public School School { get; set; }
    }

    [TestFixture]
    public class DataFacadeTests
    {

        [Test]
        public void GetData_ShouldReturnCorrectData()
        {
            var dqMock = new Mock<IDataQuery>();
            dqMock.Setup(x => x.GetMapping()).Returns(GetPersonMapping());

            var adapterMock = new Mock<IDataAdapter>();
            adapterMock.Setup(x => x.GetData(dqMock.Object.GetMapping().TableName, dqMock.Object.ToSql()))
                .Returns(GetRawData());

            var dataFacade = new DatabaseDataFacade(adapterMock.Object);

            string expected;
            string actual;

            var serializer = new XmlSerializer(typeof(List<Person>));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, GetPeople());
                expected = writer.ToString();
                writer.GetStringBuilder().Clear();

                serializer.Serialize(writer, dataFacade.GetTableData<Person>(dqMock.Object));
                actual = writer.ToString();
            }

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetJoinedData_ShouldReturnCorrectData()
        {
            var dqMock = new Mock<IDataQuery>();
            dqMock.Setup(x => x.GetMapping()).Returns(GetPersonMapping());
            dqMock.Setup(x => x.GetJoinedMapping()).Returns(GetSchoolMapping());

            var adapterMock = new Mock<IDataAdapter>();
            adapterMock.Setup(x => x.GetData(dqMock.Object.GetMapping().TableName, dqMock.Object.ToSql()))
                .Returns(GetRawJoinedData());

            var dataFacade = new DatabaseDataFacade(adapterMock.Object);

            string expected;
            string actual;

            var serializer = new XmlSerializer(typeof(List<Person>));
            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer, GetJoinedPeople());
                expected = writer.ToString();
                writer.GetStringBuilder().Clear();

                serializer.Serialize(writer, dataFacade.GetJoinedTableData<Person, School>(dqMock.Object, "School"));
                actual = writer.ToString();
            }

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void UpdateData_ShouldComplete()
        {
            var dqMock = new Mock<IDataQuery>();
            dqMock.Setup(x => x.GetMapping()).Returns(GetPersonMapping());

            var adapterMock = new Mock<IDataAdapter>();
            adapterMock.Setup(x => x.UpdateData(It.IsAny<Dictionary<string, object>>(), 
                It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);

            var dataFacade = new DatabaseDataFacade(adapterMock.Object);
            var result = dataFacade.ModifyObject<Person>(GetPeople()[0], dqMock.Object);

            Assert.IsTrue(result);
        }

        [Test]
        public void UpdateData_ShouldFailWithNullQuery()
        {
            var adapterMock = new Mock<IDataAdapter>();
            adapterMock.Setup(x => x.UpdateData(It.IsAny<Dictionary<string, object>>(),
                It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);

            var dataFacade = new DatabaseDataFacade(adapterMock.Object);
            Assert.Throws<ArgumentNullException>(() => dataFacade.ModifyObject<Person>(GetPeople()[0], null));
        }

        [Test]
        public void DeleteData_ShouldComplete()
        {
            var dqMock = new Mock<IDataQuery>();
            dqMock.Setup(x => x.GetMapping()).Returns(GetPersonMapping());

            var adapterMock = new Mock<IDataAdapter>();
            adapterMock.Setup(x => x.DeleteData(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            var dataFacade = new DatabaseDataFacade(adapterMock.Object);
            var result = dataFacade.DeleteObject(dqMock.Object);

            Assert.IsTrue(result);
        }

        [Test]
        public void DeleteData_ShouldFailWithNullQuery()
        {
            var adapterMock = new Mock<IDataAdapter>();
            adapterMock.Setup(x => x.DeleteData(It.IsAny<string>(), It.IsAny<string>())).Returns(true);

            var dataFacade = new DatabaseDataFacade(adapterMock.Object);
            Assert.Throws<ArgumentNullException>(() => dataFacade.DeleteObject(null));
        }

        [Test]
        public void GetTableCount_ShouldComplete()
        {
            var expected = 10;

            var dqMock = new Mock<IDataQuery>();
            dqMock.Setup(x => x.GetMapping()).Returns(GetPersonMapping());

            var adapterMock = new Mock<IDataAdapter>();
            adapterMock.Setup(x => x.GetTableRowCount(It.IsAny<string>(), It.IsAny<string>())).Returns(expected);


            var dataFacade = new DatabaseDataFacade(adapterMock.Object);
            var actual = dataFacade.GetTableRowCount(dqMock.Object);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetTableCount_ShouldFailWithNullQuery()
        {
            var expected = 10;

            var adapterMock = new Mock<IDataAdapter>();
            adapterMock.Setup(x => x.GetTableRowCount(It.IsAny<string>(), It.IsAny<string>())).Returns(expected);

            var dataFacade = new DatabaseDataFacade(adapterMock.Object);
            Assert.Throws<ArgumentNullException>(() => dataFacade.GetTableRowCount(null));
        }
        

        private List<Dictionary<string, object>> GetRawData()
        {
            var first = new Dictionary<string, object>();
            first.Add("person_id", 1);
            first.Add("first_name", "Ivan");
            first.Add("last_name", "Ivanov");
            first.Add("age", 15);
            first.Add("school_id", 1);

            var second = new Dictionary<string, object>();
            second.Add("person_id", 2);
            second.Add("first_name", "Petar");
            second.Add("last_name", "Petrov");
            second.Add("age", 45);
            second.Add("school_id", 2);

            var third = new Dictionary<string, object>();
            third.Add("person_id", 3);
            third.Add("first_name", "Gosho");
            third.Add("last_name", "Georgiev");
            third.Add("age", 43);
            third.Add("school_id", 3);

            return new List<Dictionary<string, object>> { first, second, third };
        }
        private List<Dictionary<string, object>> GetRawJoinedData()
        {
            var rawData = GetRawData();
            rawData[0].Add("id", 1);
            rawData[0].Add("name", "PMG");
            rawData[1].Add("id", 2);
            rawData[1].Add("name", "EG");
            rawData[2].Add("id", 3);
            rawData[2].Add("name", "PGI");

            return rawData;
        }
        private List<Person> GetPeople()
        {
            var first = new Person { PersonId = 1, FirstName = "Ivan", LastName = "Ivanov", Age = 15, SchoolId = 1 };
            var second = new Person { PersonId = 2, FirstName = "Petar", LastName = "Petrov", Age = 45, SchoolId = 2 };
            var third = new Person { PersonId = 3, FirstName = "Gosho", LastName = "Georgiev", Age = 43, SchoolId = 3 };

            return new List<Person> { first, second, third };
        }
        private List<Person> GetJoinedPeople()
        {
            var people = GetPeople();
            var schools = GetSchools();
            for(int i=0; i<people.Count; i++)
            {
                people[i].School = schools[i];
            }

            return people;
        }
        private List<School> GetSchools()
        {
            var first = new School { Id = 1, Name = "PMG" };
            var second = new School { Id = 2, Name = "EG" };
            var third = new School { Id = 3, Name = "PGI" };

            return new List<School> { first, second, third };
        }
        private TableMapping GetPersonMapping()
        {
            var pairs = new List<Pair>
            {
                new Pair
                {
                    DataSource = "person_id",
                    Model = "PersonId"
                },
                new Pair
                {
                    DataSource = "first_name",
                    Model = "FirstName"
                },
                new Pair
                {
                    DataSource = "last_name",
                    Model = "LastName"
                },
                new Pair
                {
                    DataSource = "age",
                    Model = "Age"
                },
                new Pair
                {
                    DataSource = "school_id",
                    Model = "SchoolId"
                }
            };
            var mapping = new TableMapping();
            mapping.Pairs = pairs;
            mapping.TableName = "People";
            return mapping;
        }
        private TableMapping GetSchoolMapping()
        {
            var pairs = new List<Pair>
            {
                new Pair
                {
                    DataSource = "id",
                    Model = "Id"
                },
                new Pair
                {
                    DataSource = "name",
                    Model = "Name"
                },              
            };
            var mapping = new TableMapping();
            mapping.Pairs = pairs;
            mapping.TableName = "Schools";
            return mapping;
        }

    }
}
