﻿using Configuration;
using DataAccessLayer;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace UnitTests.DataAccessLayer
{
    [TestFixture]
    public class DataQueryTests
    {
        private static string[] searchProperties = { "FirstName", "LastName" };

        [TestCaseSource("Cases")]
        public void ToSql_ShouldReturnCorrectStatement(
            string tableName,
            Dictionary<string, object> queries,
            string[] searchProperties,
            string search,
            string sort,
            bool? isAsc,
            int? page,
            int? elements,
            string expectedSql)
        {
            var dq = new DataQuery();
            dq.Init(getMapping())
            .SortBy(sort, isAsc)
            .GetPage(page, elements)
            .SearchBy(searchProperties, search);

            foreach (var query in queries)
            {
                dq.AddQuery(query.Key, query.Value);
            }
            var actual = dq.ToSql();

            Assert.AreEqual(expectedSql, actual);
        }

        [Test]
        public void Init_InitWrongMapping()
        {
            var dq = new DataQuery();
            TableMapping mapping = null;
            Assert.Throws<ArgumentException>(() => dq.Init(mapping));
        }

        private static Dictionary<string,object> getQueries()
        {
            var queries = new Dictionary<string, object>();
            queries.Add("Id", 1);
            queries.Add("FirstName", "Ivan");
            queries.Add("LastName", "Ivanov");
            queries.Add("Age", 24);
            return queries;
        }

        private static TableMapping getMapping()
        {
            var pairs =  new List<Pair>
            {
                new Pair
                {
                    DataSource = "id",
                    Model = "Id"
                },
                new Pair
                {
                    DataSource = "first_name",
                    Model = "FirstName"
                },
                new Pair
                {
                    DataSource = "last_name",
                    Model = "LastName"
                },
                new Pair
                {
                    DataSource = "age",
                    Model = "Age"
                }
            };
            var mapping = new TableMapping();
            mapping.Pairs = pairs;
            mapping.TableName = "Documents";
            return mapping;
        }

        static object[] Cases =
        {
            new object[] {"Documents", getQueries(), null, null, null, null, null, null,
                " WHERE id=1 AND first_name=Ivan AND last_name=Ivanov AND age=24" },

            new object[] {"Documents", getQueries(), searchProperties, null, null, null, null, null,
                " WHERE id=1 AND first_name=Ivan AND last_name=Ivanov AND age=24" },

            new object[] {"Documents", getQueries(), searchProperties, "smth", null, null, null, null,
                " WHERE id=1 AND first_name=Ivan AND last_name=Ivanov AND age=24 AND (first_name LIKE '%smth%' OR last_name LIKE '%smth%')" },

            new object[] {"Documents", getQueries(), searchProperties, "smth", null, null, null, null,
               " WHERE id=1 AND first_name=Ivan AND last_name=Ivanov AND age=24 AND (first_name LIKE '%smth%' OR last_name LIKE '%smth%')" },

            new object[] {"Documents", getQueries(), searchProperties, "smth", "FirstName", null, null, null,
               " WHERE id=1 AND first_name=Ivan AND last_name=Ivanov AND age=24 AND (first_name LIKE '%smth%' OR last_name LIKE '%smth%') ORDER BY first_name ASC" },

            new object[] {"Documents", getQueries(), searchProperties, "smth", "FirstName", false, null, null,
               " WHERE id=1 AND first_name=Ivan AND last_name=Ivanov AND age=24 AND (first_name LIKE '%smth%' OR last_name LIKE '%smth%') ORDER BY first_name DESC" },

            new object[] {"Documents", getQueries(), searchProperties, "smth", "FirstName", false, 1, null,
              " WHERE id=1 AND first_name=Ivan AND last_name=Ivanov AND age=24 AND (first_name LIKE '%smth%' OR last_name LIKE '%smth%') ORDER BY first_name DESC OFFSET((1 - 1) * 5) ROWS FETCH NEXT 5 ROWS ONLY" },

            new object[] {"Documents", getQueries(), searchProperties, "smth", "FirstName", false, 1, 20,
             " WHERE id=1 AND first_name=Ivan AND last_name=Ivanov AND age=24 AND (first_name LIKE '%smth%' OR last_name LIKE '%smth%') ORDER BY first_name DESC OFFSET((1 - 1) * 20) ROWS FETCH NEXT 20 ROWS ONLY" },
        };
    }
}
