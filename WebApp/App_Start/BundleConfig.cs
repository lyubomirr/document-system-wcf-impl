﻿using System.Web.Optimization;

namespace WebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/lib/jquery-{version}.js",
                       "~/Scripts/lib/MicrosoftAjax.js",
                       "~/Scripts/lib/promissory-arbiter.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/lib/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/lib/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/lib/bootstrap.bundle.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                       "~/Scripts/lib/knockout-min.js"));

            bundles.Add(new ScriptBundle("~/bundles/AppInit").Include(
                       "~/Scripts/init/AppInit.js"));

            bundles.Add(new ScriptBundle("~/bundles/src")
                .IncludeDirectory("~/Scripts/src/", "*.js", true)
                );

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            
        }
    }
}
