﻿using Ninject.Modules;
using Interfaces.Repositories;
using RepositoryLayer;

namespace WebApp.NinjectModules
{
    public class RepositoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDocumentsRepository>().To<DocumentsRepository>()
                .WithConstructorArgument("tableName", "Documents");

            Bind<IDocumentTypesRepository>().To<DocumentTypesRepository>()
                .WithConstructorArgument("tableName", "DocumentTypes");

            Bind<IFileCabinetsRepository>().To<FileCabinetsRepository>()
                .WithConstructorArgument("tableName", "FileCabinets");

            Bind<IRolesRepository>().To<RolesRepository>()
                .WithConstructorArgument("tableName", "Roles");

            Bind<IUsersRepository>().To<UsersRepository>()
                .WithConstructorArgument("tableName", "Users");
        }
    }
}