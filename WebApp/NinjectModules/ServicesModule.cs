﻿using BusinessLayer.Services;
using Interfaces.Services;
using Ninject.Modules;
using System.Configuration;
using System.Web;

namespace WebApp.NinjectModules
{
    public class ServicesModule : NinjectModule
    {
        public override void Load()
        {
            var uploadsFullPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["uploadDirectory"]);
            var tempFullPath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["tempDirectory"]);

            Bind<IDocumentsService>().To<DocumentsService>()
                .WithConstructorArgument("uploadsDirectoryPath", uploadsFullPath)
                .WithConstructorArgument("tempFilesDirectoryPath", tempFullPath);

            Bind<IFileCabinetsService>().To<FileCabinetsService>()
                .WithConstructorArgument("uploadsDirectoryPath", uploadsFullPath);

            Bind<IDocumentTypesService>().To<DocumentTypesService>();
            Bind<IRolesService>().To<RolesService>();
            Bind<IUsersService>().To<UsersService>();
        }
        
        //private ServiceT createService<ServiceT>(string serviceRelativePath)
        //{
        //    WebHttpBinding binding = new WebHttpBinding
        //    {
        //        MaxReceivedMessageSize = int.MaxValue,
        //        MaxBufferSize = int.MaxValue
        //    };

        //    EndpointAddress endpointAddress = new EndpointAddress(
        //        $"{ConfigurationManager.AppSettings["baseUrl"]}{serviceRelativePath}");

        //    var channelFactory = new ChannelFactory<ServiceT>(binding, endpointAddress);
        //    channelFactory.Endpoint.EndpointBehaviors.Add(new WebScriptEnablingBehavior());

        //    return channelFactory.CreateChannel();
        //}
    }
}