﻿using System;
using System.Web.Mvc;
using WebApp.Authorize;
using Interfaces.Services;
using System.Linq;
using WebApp.Helpers;
using System.IO;

namespace WebApp.Controllers
{
    [AuthorizeUser]
    public class DocumentsController : Controller
    {
        private IDocumentsService _documentsService;

        public DocumentsController(IDocumentsService documentsService)
        {
            _documentsService = documentsService;
        }

        [AuthorizeNotGuest]
        [HttpPost]
        [Route("documents/uploadchunks/{guid}")]
        public ActionResult UploadChunks(string guid, string fileName)
        {
            if (Request.Files.Count == 0)
            {
                return new EmptyResult();
            }

            _documentsService.UploadChunks(guid, fileName, Request.Files[0].InputStream);
            return new EmptyResult();
        }

    }
}