﻿using System.Collections;
using System.Globalization;
using System.Web.Mvc;
using System.Linq;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetCurrentResources()
        {
            return Json(
                Resources.Data.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true)
                .OfType<DictionaryEntry>().ToDictionary(el => el.Key.ToString(), el => el.Value.ToString()), 
                JsonRequestBehavior.AllowGet);
        }
    }
}