﻿using Interfaces.Data;
using Interfaces.Services;
using Interfaces.Wcf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApp.Authorize;

namespace WebApp.Controllers
{
    [AuthorizeUser]
    public class FileCabinetsController : Controller
    {
        private IDocumentsService _documentsService;

        public FileCabinetsController(IDocumentsService documentsService)
        {
            _documentsService = documentsService;
        }

        [AuthorizeNotGuest]
        [Route("filecabinets/export/{guid}")]
        public ActionResult Export(string guid, string filename)
        {
            var stream = _documentsService.ExportDocumentsFromFileCabinet(Guid.Parse(guid));
            return File(stream, "application/zip", filename + ".zip");
        }
    }
}