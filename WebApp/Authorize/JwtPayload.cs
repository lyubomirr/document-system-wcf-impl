﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Authorize
{
    public class JwtPayload
    {
        public Guid Guid { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public long exp { get; set; }
    }
}