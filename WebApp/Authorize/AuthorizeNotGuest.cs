﻿using JWT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Authorize
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeNotGuest : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            try
            {
                if (httpContext.Request.Cookies["jwt"] == null)
                {
                    return false;
                }

                var payload = Jwt.DecodeToken(httpContext.Request.Cookies["jwt"].Value);
                return payload.Role == "Admin" || payload.Role == "Regular";
    
            }
            catch (TokenExpiredException)
            {
                return false;
            }
            catch (SignatureVerificationException)
            {
                return false;
            }
        }
    }
}