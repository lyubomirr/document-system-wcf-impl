﻿using JWT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.Authorize;

namespace WebApp.Helpers
{
    public static class Utils
    {
        public static bool isAuthenticated(HttpRequestBase request)
        {
            try
            {
                if(request.Cookies["jwt"] == null)
                {
                    return false;
                }

                Jwt.DecodeToken(request.Cookies["jwt"].Value);
                return true;
            }
            catch (TokenExpiredException)
            {
                return false;
            }
            catch (SignatureVerificationException)
            {
                return false;
            }
        }
    }
}