﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace WebApp.Helpers
{
    public class Hashing
    {
        private const int _saltSize = 4;
        private HashAlgorithm _hashAlgorithm;

        public Hashing(HashAlgorithm hashAlgorithm)
        {
            _hashAlgorithm = hashAlgorithm;
        }

        public string ComputeHash(string plainText, string salt)
        {
            byte[] saltBytes = Encoding.UTF8.GetBytes(salt);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            byte[] plainTextAndSalt = new byte[plainTextBytes.Length + salt.Length];

            for(int i = 0; i < plainTextBytes.Length; i++)
            {
                plainTextAndSalt[i] = plainTextBytes[i];
            }

            for (int i = 0; i < saltBytes.Length; i++)
            {
                plainTextAndSalt[i + plainTextBytes.Length] = saltBytes[i];
            }

            byte[] hashed = _hashAlgorithm.ComputeHash(plainTextAndSalt);

            StringBuilder hashedString = new StringBuilder();
            foreach (byte singleByte in hashed)
            {
                hashedString.Append(singleByte.ToString("x2"));
            }
            return hashedString.ToString();
        }

        public string GenerateSalt()
        {
            byte[] salt = new byte[_saltSize];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetNonZeroBytes(salt);
            }

            StringBuilder saltString = new StringBuilder();
            foreach (byte singleByte in salt)
            {
                saltString.Append(singleByte.ToString("x2"));
            }
            return saltString.ToString();
        }

        public bool VerifyHash(string newPlainText, string hashValue, string salt)
        {
            return ComputeHash(newPlainText, salt) == hashValue;
        }
    }
}