﻿using DataAccessLayer;
using DatabaseConnectionLayer;
using Interfaces.Data;
using System;
using System.Configuration;

namespace WebApp.Factories
{
    public class DataAccessFactory
    {
        public IDataAdapter CreateDataAdapter(string dbType)
        {

            switch (dbType)
            {
                case "mysql":
                    {
                        string connectionString =
                            ConfigurationManager.ConnectionStrings["MySqlConnection"].ConnectionString;

                        if (String.IsNullOrEmpty(connectionString))
                        {
                            throw new ArgumentNullException("connectionString");
                        }
                        return new MySqlDataAdapter(connectionString);
                    }
                case "sqlserver":
                    {
                        string connectionString =
                             ConfigurationManager.ConnectionStrings["SqlServerConnection"].ConnectionString;

                        if (String.IsNullOrEmpty(connectionString))
                        {
                            throw new ArgumentNullException("connectionString");
                        }
                        return new SqlServerDataAdapter(connectionString);
                    }
                default:
                    {
                        throw new ArgumentException("Db type not supported!");
                    }
            }
        }

        public IDataQuery CreateDataQuery(string dbType)
        {
            switch (dbType)
            {
                case "mysql":
                    {
                        return new MySqlDataQuery();
                    }
                case "sqlserver":
                    {
                        return new SqlServerDataQuery();
                    }
                default:
                    {
                        throw new ArgumentException("Db type not supported!");
                    }
            }
        }
    }
}