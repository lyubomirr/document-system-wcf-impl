﻿using Ninject;
using Ninject.Extensions.Wcf;
using System.ServiceModel;
using WebApp.NinjectModules;

namespace WebApp.Factories
{
    public class NinjectFileLessServiceHostFactory : NinjectServiceHostFactory
    {
        public NinjectFileLessServiceHostFactory()
        {
            var kernel = new StandardKernel(new DalModule(), new ServicesModule(), new RepositoryModule() );
            kernel.Bind<ServiceHost>().To<NinjectServiceHost>();
            SetKernel(kernel);
        }
    }
}
