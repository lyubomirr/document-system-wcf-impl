var Api;
(function (Api) {
    var WcfEndpoints = /** @class */ (function () {
        function WcfEndpoints() {
            this.GetDocuments = "GetDocuments";
            this.GetDeleted = "GetDeleted";
            this.GetArchive = "GetArchive";
        }
        return WcfEndpoints;
    }());
    Api.WcfEndpoints = WcfEndpoints;
})(Api || (Api = {}));
//# sourceMappingURL=WcfEndpoints.js.map