var Services;
(function (Services) {
    var Config = /** @class */ (function () {
        function Config() {
            this.supportedPreviewExtensions = [".pdf", ".png", ".jpg", ".jpeg", ".bmp", ".txt", ".gif", ".js", ".css"];
            this.currentDataCount = ko.observable(0);
            this.pageElementsAvailableOptions = [1, 2, 5, 10, 20, 50, 100];
            this.bytesPerUploadChunk = 4000000; //4000000
        }
        return Config;
    }());
    Services.Config = Config;
})(Services || (Services = {}));
//# sourceMappingURL=ApplicationConfig.js.map