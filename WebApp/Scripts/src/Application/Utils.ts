﻿namespace Services {
    export class Utils {
        public hideModal(identifier: string): void {
            $(identifier).modal("hide");
        }

        public showModal(identifier: string): void {
            $(identifier).modal("show");
        }

        public isInputValid(value: string): boolean {
            return (value.length > 0 && value.trim().length > 0);
        }

        public alertInvalidDataError(alertIdentifier: string, errorInfo: string): void {
            $(alertIdentifier).find(".error-info").text(errorInfo);

            $(alertIdentifier).slideDown("ease");
            setTimeout(() => {
                $(alertIdentifier).slideUp("ease");
            }, 1500);
        }

        public getFileExtension(fileName: string) {
            let re = /(?:\.([^.]+))?$/;
            return re.exec(fileName)[0];
        }

        public hideProgressBar() {
            $(".progress").slideUp();
            $(".upload-file-progress").attr('aria-valuenow', 0).css('width', 0 + "%");
        }

        public parseJsonDate(date: string): Date {
            if (date.indexOf("Date") != -1) {
                return new Date(+date.match(/-?\d+/)[0]);
            }
            return new Date(date);
        }

        public checkIfUnautorized(error: WcfServices.WcfErrorContract) {
            if (error.get_statusCode() == 401) {
                App.Service.currentUser(null);
                window.location.href = "/";
            }
        }
    }
}