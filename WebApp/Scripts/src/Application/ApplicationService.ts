﻿namespace Services {
    export class ApplicationService {
        public connManager: Api.ConnectionManager = new Api.ConnectionManager();
        public wcf: Api.WcfConnectionManager = new Api.WcfConnectionManager();
        public endpoints: Api.Endpoints = new Api.Endpoints();
        public dataMapper: Api.DataMap;
        public currentUser: KnockoutObservable<Models.User> = ko.observable();
        public locResources: Dictionary<string>;

        public getResources(): JQueryPromise<void> {
            let dfd = $.Deferred<void>();
            this.connManager.invokeGet<Dictionary<string>>(App.Service.endpoints.getCurrentResources)
                .then((resources: Dictionary<string>) => {
                    this.locResources = resources;
                    dfd.resolve();
                })

            return dfd.promise();
        }

        public init(): JQueryPromise<void> {
            let dfd = $.Deferred<void>();

            this.wcf.rolesService.GetRoles()
                .then((roles: Models.Role[]) => {
                    this.wcf.docTypesService.GetDocumentTypes()
                        .then((docTypes: Models.DocumentType[]) => {
                            for (let docType of docTypes) {
                                docType.Name = this.locResources[docType.Name];
                            }

                            this.dataMapper = new Api.DataMap(roles, docTypes);
                            dfd.resolve();
                        });
                });       

            return dfd.promise();
        }
    }
}
