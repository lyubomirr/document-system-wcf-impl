var Services;
(function (Services) {
    var Utils = /** @class */ (function () {
        function Utils() {
        }
        Utils.prototype.hideModal = function (identifier) {
            $(identifier).modal("hide");
        };
        Utils.prototype.showModal = function (identifier) {
            $(identifier).modal("show");
        };
        Utils.prototype.isInputValid = function (value) {
            return (value.length > 0 && value.trim().length > 0);
        };
        Utils.prototype.alertInvalidDataError = function (alertIdentifier, errorInfo) {
            $(alertIdentifier).find(".error-info").text(errorInfo);
            $(alertIdentifier).slideDown("ease");
            setTimeout(function () {
                $(alertIdentifier).slideUp("ease");
            }, 1500);
        };
        Utils.prototype.getFileExtension = function (fileName) {
            var re = /(?:\.([^.]+))?$/;
            return re.exec(fileName)[0];
        };
        Utils.prototype.hideProgressBar = function () {
            $(".progress").slideUp();
            $(".upload-file-progress").attr('aria-valuenow', 0).css('width', 0 + "%");
        };
        Utils.prototype.parseJsonDate = function (date) {
            if (date.indexOf("Date") != -1) {
                return new Date(+date.match(/-?\d+/)[0]);
            }
            return new Date(date);
        };
        Utils.prototype.checkIfUnautorized = function (error) {
            if (error.get_statusCode() == 401) {
                App.Service.currentUser(null);
                window.location.href = "/";
            }
        };
        return Utils;
    }());
    Services.Utils = Utils;
})(Services || (Services = {}));
//# sourceMappingURL=Utils.js.map