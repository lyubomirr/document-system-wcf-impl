var Api;
(function (Api) {
    var Endpoints = /** @class */ (function () {
        function Endpoints() {
            this.exportFileCabinet = "Services/WcfFileServiceHost.svc/ExportFileCabinet";
            this.exportDocuments = "Services/WcfFileServiceHost.svc/ExportDocuments";
            this.getFile = "Services/WcfFileServiceHost.svc/GetFile";
            this.uploadChunks = "Documents/UploadChunks/";
            this.getCurrentResources = "home/getcurrentresources";
        }
        return Endpoints;
    }());
    Api.Endpoints = Endpoints;
})(Api || (Api = {}));
//# sourceMappingURL=Endpoints.js.map