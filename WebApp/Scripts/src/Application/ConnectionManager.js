var Api;
(function (Api) {
    var ConnectionManager = /** @class */ (function () {
        function ConnectionManager() {
        }
        ConnectionManager.prototype.getJsonRequest = function (apiUrl) {
            return $.ajax({
                method: 'GET',
                url: apiUrl,
                contentType: "application/json"
            });
        };
        ConnectionManager.prototype.postJsonRequest = function (apiUrl, sentData) {
            return $.post(apiUrl, sentData);
        };
        ConnectionManager.prototype.invokeGet = function (apiUrl, query) {
            var dfd = $.Deferred();
            var finalUrl = apiUrl;
            if (query) {
                finalUrl += query;
            }
            this.getJsonRequest(finalUrl)
                .then(function (data) {
                dfd.resolve(data);
            });
            return dfd.promise();
        };
        ConnectionManager.prototype.invokePost = function (apiUrl, sentData, query) {
            var dfd = $.Deferred();
            var finalUrl = apiUrl;
            if (query) {
                finalUrl += query;
            }
            this.postJsonRequest(finalUrl, sentData)
                .then(function (data) {
                dfd.resolve(data);
            });
            return dfd.promise();
        };
        ConnectionManager.prototype.invokePostDifferent = function (apiUrl, sentData, query) {
            var dfd = $.Deferred();
            var finalUrl = apiUrl;
            if (query) {
                finalUrl += query;
            }
            this.postJsonRequest(finalUrl, sentData)
                .then(function (data) {
                dfd.resolve(data);
            });
            return dfd.promise();
        };
        ConnectionManager.prototype.uploadFileByChunks = function (uploadChunkUrl, blob) {
            var dfd = $.Deferred();
            var fileSize = blob.size;
            var startChunk = 0;
            var endChunk = App.Config.bytesPerUploadChunk;
            $(".progress").slideDown();
            this.uploadChunks(uploadChunkUrl, startChunk, endChunk, blob, dfd);
            return dfd.promise();
        };
        ConnectionManager.prototype.uploadChunks = function (url, startChunk, endChunk, blob, dfd) {
            var _this = this;
            if (startChunk >= blob.size) {
                dfd.resolve(blob.name);
                return;
            }
            var chunk = blob.slice(startChunk, endChunk);
            var fd = new FormData();
            fd.append("chunk", chunk);
            fd.append("fileName", blob.name);
            $.ajax({
                url: url,
                type: 'POST',
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                success: function () {
                    var percentComplete = (endChunk / blob.size) * 100;
                    $(".upload-file-progress").attr('aria-valuenow', percentComplete).css('width', percentComplete + "%");
                    startChunk = endChunk;
                    endChunk += App.Config.bytesPerUploadChunk;
                    _this.uploadChunks(url, startChunk, endChunk, blob, dfd);
                },
                error: function () {
                    dfd.reject();
                    return;
                }
            });
        };
        return ConnectionManager;
    }());
    Api.ConnectionManager = ConnectionManager;
})(Api || (Api = {}));
//# sourceMappingURL=ConnectionManager.js.map