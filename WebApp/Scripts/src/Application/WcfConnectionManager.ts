﻿namespace Api {
    export class WcfConnectionManager {
        public documentsService = new WcfWrap.DocumentsService();
        public usersService = new WcfWrap.UsersService();
        public fileCabinetsService = new WcfWrap.FileCabinetsService();
        public rolesService = new WcfWrap.RolesService();
        public docTypesService = new WcfWrap.DocumentTypesService();
        public appService = new WcfWrap.AppService();
    };
}
