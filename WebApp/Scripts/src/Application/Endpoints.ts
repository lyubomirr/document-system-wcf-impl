﻿namespace Api {
    export class Endpoints {
        public readonly exportFileCabinet: string = "Services/WcfFileServiceHost.svc/ExportFileCabinet";
        public readonly exportDocuments: string = "Services/WcfFileServiceHost.svc/ExportDocuments";   
        public readonly getFile: string = "Services/WcfFileServiceHost.svc/GetFile";     
        public readonly uploadChunks: string = "Documents/UploadChunks/";
        public readonly getCurrentResources: string = "home/getcurrentresources";
    }
}