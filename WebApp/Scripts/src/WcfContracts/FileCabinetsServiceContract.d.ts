﻿declare module WcfServices {
    export class IWcfFileCabinetsService {
        GetFileCabinets(searchQuery: Models.SearchQuery, succeededCallback: CallbackFunction,
            failedCallback: CallbackFunction);

        GetCount(searchQuery: Models.SearchQuery, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
        Add(fileCabinet: Models.FileCabinet, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
        Delete(guid: string, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
        Archivate(guid: string, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
        Update(fileCabinet: Models.FileCabinet, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
    }
}