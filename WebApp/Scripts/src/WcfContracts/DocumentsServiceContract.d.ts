﻿declare module WcfServices {
    export class IWcfDocumentsService {
        GetDocuments(fcGuid: string, searchQuery: Models.SearchQuery,
            succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        GetDeleted(searchQuery: Models.SearchQuery, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        GetArchive(searchQuery: Models.SearchQuery, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        GetCount(fcGuid: string, searchQuery: Models.SearchQuery,
            succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        GetDeletedCount(searchQuery: Models.SearchQuery,
            succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        GetArchiveCount(searchQuery: Models.SearchQuery,
            succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        Delete(guids: string[], succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        Update(doc: Models.Document, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        Archivate(guid: string, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        RestoreArchivated(guids: string[], succeededCallback: CallbackFunction,
            failedCallback: CallbackFunction);

        RestoreAllArchivated(succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        RestoreDeleted(guids: string[], succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        RestoreAllDeleted(succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        Copy(doc: Models.Document, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        CompleteUpload(fileInfo: Models.UploadedFileInfo, succeededCallback: CallbackFunction,
            failedCallback: CallbackFunction);

        Add(doc: Models.Document, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);

        CompleteImport(importInfo: Models.UploadedFileInfo, succeededCallback: CallbackFunction,
            failedCallback: CallbackFunction);

    }
}