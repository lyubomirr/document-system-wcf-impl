﻿declare module WcfServices {
    export class IWcfUsersService {
        CheckAuthorization(succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
        Login(loggedUser: Models.User, succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
        Logout(succeededCallback: CallbackFunction, failedCallback: CallbackFunction);
    }
}