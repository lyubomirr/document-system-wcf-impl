﻿namespace WcfWrap {
    export class RolesService {
        private wcfService = new WcfServices.IWcfRolesService();

        public GetRoles(): JQueryPromise<Models.Role[]> {
            let dfd: JQueryDeferred<Models.Role[]> = $.Deferred();

            this.wcfService.GetRoles((roles: Models.Role[]) => {
                dfd.resolve(roles);
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }
    }
}