﻿namespace WcfWrap {
    export class UsersService {
        private wcfService = new WcfServices.IWcfUsersService();

        public CheckAuthorization() {
            let dfd: JQueryDeferred<Models.User> = $.Deferred();
            this.wcfService.CheckAuthorization((user: Models.User) => {
                dfd.resolve(user);
            }, () => {
                dfd.reject(false);
            });
            return dfd.promise();
        }

        public Login(loggedUser: Models.User) {
            let dfd: JQueryDeferred<Models.User> = $.Deferred();
            this.wcfService.Login(loggedUser, (user: Models.User) => {
                dfd.resolve(user);
            }, (err: WcfServices.WcfErrorContract) => {
                dfd.reject(err);
                console.log(err);
            });
            return dfd.promise();
        }

        public Logout() {
            let dfd: JQueryDeferred<void> = $.Deferred();
            this.wcfService.Logout(() => {
                dfd.resolve();
            }, () => {
                dfd.reject();
            });
            return dfd.promise();
        }
    }
}