﻿namespace WcfWrap {
    export class DocumentsService {
        private wcfService = new WcfServices.IWcfDocumentsService();

        public GetDocuments(fcGuid: string, searchQuery: Models.SearchQuery): JQueryPromise<Models.Document[]> {

            let dfd: JQueryDeferred<Models.Document[]> = $.Deferred();
            this.wcfService.GetDocuments(fcGuid, searchQuery, (docs) => {
                    dfd.resolve(docs);
                }, (err: WcfServices.WcfErrorContract) => {
                    App.Utils.checkIfUnautorized(err);
                    dfd.reject(err);
                });
            return dfd.promise();
        };

        public GetArchive(searchQuery: Models.SearchQuery): JQueryPromise<Models.Document[]> {

            let dfd: JQueryDeferred<Models.Document[]> = $.Deferred();
            this.wcfService.GetArchive(searchQuery, (docs) => {
                    dfd.resolve(docs);
                }, (err: WcfServices.WcfErrorContract) => {
                    App.Utils.checkIfUnautorized(err);
                    dfd.reject(err);
                }
            );
            return dfd.promise();
        };

        public GetDeleted(searchQuery: Models.SearchQuery): JQueryPromise<Models.Document[]> {

            let dfd: JQueryDeferred<Models.Document[]> = $.Deferred();
            this.wcfService.GetDeleted(searchQuery, (docs) => {
                    dfd.resolve(docs);
                }, (err: WcfServices.WcfErrorContract) => {
                    App.Utils.checkIfUnautorized(err);
                    dfd.reject(err);
                }
            );
            return dfd.promise();
        };

        public GetCount(fcGuid: string, searchQuery?: Models.SearchQuery): JQueryPromise<number> {
            let dfd: JQueryDeferred<number> = $.Deferred();

            this.wcfService.GetCount(fcGuid, searchQuery, (count: number) => {
                    dfd.resolve(count);
                }, (err: WcfServices.WcfErrorContract) => {
                    App.Utils.checkIfUnautorized(err);
                    dfd.reject(err);
                }
            );
            return dfd.promise();
        }

        public GetDeletedCount(searchQuery?: Models.SearchQuery): JQueryPromise<number> {
            let dfd: JQueryDeferred<number> = $.Deferred();

            this.wcfService.GetDeletedCount(searchQuery, (count: number) => {
                    dfd.resolve(count);
                }, (err: WcfServices.WcfErrorContract) => {
                    App.Utils.checkIfUnautorized(err);
                    dfd.reject(err);
                }
            );
            return dfd.promise();
        }

        public GetArchiveCount(searchQuery?: Models.SearchQuery): JQueryPromise<number> {
            let dfd: JQueryDeferred<number> = $.Deferred();

            this.wcfService.GetArchiveCount(searchQuery, (count: number) => {
                    dfd.resolve(count);
                }, (err: WcfServices.WcfErrorContract) => {
                    App.Utils.checkIfUnautorized(err);
                    dfd.reject(err);
                }
            );
            return dfd.promise();
        }

        public Delete(guids: string[]): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();

            this.wcfService.Delete(guids, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject();
            });

            return dfd.promise();
        }

        public Update(doc: Models.Document): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();

            this.wcfService.Update(doc, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        public Archivate(guid: string): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();

            this.wcfService.Archivate(guid, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        public RestoreArchivated(guids: string[]): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();

            this.wcfService.RestoreArchivated(guids, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        public RestoreAllArchivated(): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();

            this.wcfService.RestoreAllArchivated(() => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        public RestoreDeleted(guids: string[]): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();

            this.wcfService.RestoreDeleted(guids, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        public RestoreAllDeleted(): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();

            this.wcfService.RestoreAllDeleted(() => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        public Copy(doc: Models.Document): JQueryPromise<Models.Document> {
            let dfd: JQueryDeferred<Models.Document> = $.Deferred();

            this.wcfService.Copy(doc, (newDoc: Models.Document) => {
                dfd.resolve(newDoc);

            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        //private UploadChunk(chunk: Models.FileUpload): JQueryPromise<void> {
        //    let dfd: JQueryDeferred<void> = $.Deferred();
        //    chunk

        //    this.wcfService.UploadChunk(chunk, () => {
        //        dfd.resolve();
        //    }, (err: WcfServices.WcfErrorContract) => {
        //        App.Utils.checkIfUnautorized(err);
        //        dfd.reject(err);
        //        console.log(err);
        //    });

        //    return dfd.promise();
        //}

        //private uploadChunks(startChunk: number, endChunk: number, guid: string, blob: File,
        //    dfd: JQueryDeferred<string>) {

        //    if (startChunk >= blob.size) {
        //        dfd.resolve(blob.name);
        //        return;
        //    }

        //    let chunkFile = blob.slice(startChunk, endChunk);
        //    let chunk = new Models.FileUpload(guid, blob.name, chunkFile);

        //    this.UploadChunk(chunk)
        //        .then(() => {
        //            var percentComplete = (endChunk / blob.size) * 100;
        //            $(".upload-file-progress").attr('aria-valuenow', percentComplete).css('width', percentComplete + "%");
        //            startChunk = endChunk;
        //            endChunk += App.Config.bytesPerUploadChunk;
        //            this.uploadChunks(startChunk, endChunk, guid, blob, dfd);
        //        }, () => {
        //            dfd.reject();
        //            return;
        //        })
        //}

        //public uploadFileByChunks(blob: File, guid: string): JQueryPromise<string> {
        //    let dfd = $.Deferred();
        //    let fileSize: number = blob.size;

        //    let startChunk: number = 0;
        //    let endChunk: number = App.Config.bytesPerUploadChunk;

        //    $(".progress").slideDown();
        //    this.uploadChunks(startChunk, endChunk, guid, blob, dfd);

        //    return dfd.promise();
        //}

        public CompleteUpload(fileInfo: Models.UploadedFileInfo): JQueryPromise<string> {
            let dfd: JQueryDeferred<string> = $.Deferred();

            this.wcfService.CompleteUpload(fileInfo, (filePath: string) => {
                dfd.resolve(filePath);
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject();
            });

            return dfd.promise();
        }

        public Add(doc: Models.Document): JQueryPromise<Models.Document> {
            let dfd: JQueryDeferred<Models.Document> = $.Deferred();

            this.wcfService.Add(doc, (returnedDoc: Models.Document) => {
                dfd.resolve(returnedDoc);
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        public CompleteImport(importInfo: Models.UploadedFileInfo): JQueryPromise<Models.Document[]> {
            let dfd: JQueryDeferred<Models.Document[]> = $.Deferred();

            this.wcfService.CompleteImport(importInfo, (docs: Models.Document[]) => {
                dfd.resolve(docs);
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }
    }
}