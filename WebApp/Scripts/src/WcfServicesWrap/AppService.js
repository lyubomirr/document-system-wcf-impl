var WcfWrap;
(function (WcfWrap) {
    var AppService = /** @class */ (function () {
        function AppService() {
            this.wcfService = new WcfServices.IWcfAppService();
        }
        AppService.prototype.GetRandomGuid = function () {
            var dfd = $.Deferred();
            this.wcfService.GetRandomGuid(function (guid) {
                dfd.resolve(guid);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        return AppService;
    }());
    WcfWrap.AppService = AppService;
})(WcfWrap || (WcfWrap = {}));
//# sourceMappingURL=AppService.js.map