﻿namespace WcfWrap {
    export class FileCabinetsService {
        private wcfService = new WcfServices.IWcfFileCabinetsService();

        public GetFileCabinets(searchQuery: Models.SearchQuery): JQueryPromise<Models.FileCabinet[]> {

            let dfd: JQueryDeferred<Models.FileCabinet[]> = $.Deferred();
            this.wcfService.GetFileCabinets(searchQuery, (fcs: Models.FileCabinet[]) => {
                    dfd.resolve(fcs);
                }, (err: WcfServices.WcfErrorContract) => {
                    App.Utils.checkIfUnautorized(err);
                    dfd.reject(err);
                });
            return dfd.promise();
        }

        public GetCount(searchQuery?: Models.SearchQuery): JQueryPromise<number> {
            let dfd: JQueryDeferred<number> = $.Deferred();
            this.wcfService.GetCount(searchQuery, (count: number) => {
                dfd.resolve(count);

            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }   

        public Add(fileCabinet: Models.FileCabinet): JQueryPromise<Models.FileCabinet> {
            let dfd: JQueryDeferred<Models.FileCabinet> = $.Deferred();
            this.wcfService.Add(fileCabinet, (returnedFc: Models.FileCabinet) => {
                dfd.resolve(returnedFc);

            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        public Delete(guid: string): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();
            this.wcfService.Delete(guid, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        public Archivate(guid: string): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();
            this.wcfService.Archivate(guid, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }

        public Update(fileCabinet: Models.FileCabinet): JQueryPromise<void> {
            let dfd: JQueryDeferred<void> = $.Deferred();
            this.wcfService.Update(fileCabinet, () => {
                dfd.resolve();
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }
    }
}