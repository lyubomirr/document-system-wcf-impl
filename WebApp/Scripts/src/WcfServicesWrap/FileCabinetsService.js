var WcfWrap;
(function (WcfWrap) {
    var FileCabinetsService = /** @class */ (function () {
        function FileCabinetsService() {
            this.wcfService = new WcfServices.IWcfFileCabinetsService();
        }
        FileCabinetsService.prototype.GetFileCabinets = function (searchQuery) {
            var dfd = $.Deferred();
            this.wcfService.GetFileCabinets(searchQuery, function (fcs) {
                dfd.resolve(fcs);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        FileCabinetsService.prototype.GetCount = function (searchQuery) {
            var dfd = $.Deferred();
            this.wcfService.GetCount(searchQuery, function (count) {
                dfd.resolve(count);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        FileCabinetsService.prototype.Add = function (fileCabinet) {
            var dfd = $.Deferred();
            this.wcfService.Add(fileCabinet, function (returnedFc) {
                dfd.resolve(returnedFc);
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        FileCabinetsService.prototype.Delete = function (guid) {
            var dfd = $.Deferred();
            this.wcfService.Delete(guid, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        FileCabinetsService.prototype.Archivate = function (guid) {
            var dfd = $.Deferred();
            this.wcfService.Archivate(guid, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        FileCabinetsService.prototype.Update = function (fileCabinet) {
            var dfd = $.Deferred();
            this.wcfService.Update(fileCabinet, function () {
                dfd.resolve();
            }, function (err) {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });
            return dfd.promise();
        };
        return FileCabinetsService;
    }());
    WcfWrap.FileCabinetsService = FileCabinetsService;
})(WcfWrap || (WcfWrap = {}));
//# sourceMappingURL=FileCabinetsService.js.map