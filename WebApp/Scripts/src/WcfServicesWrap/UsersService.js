var WcfWrap;
(function (WcfWrap) {
    var UsersService = /** @class */ (function () {
        function UsersService() {
            this.wcfService = new WcfServices.IWcfUsersService();
        }
        UsersService.prototype.CheckAuthorization = function () {
            var dfd = $.Deferred();
            this.wcfService.CheckAuthorization(function (user) {
                dfd.resolve(user);
            }, function () {
                dfd.reject(false);
            });
            return dfd.promise();
        };
        UsersService.prototype.Login = function (loggedUser) {
            var dfd = $.Deferred();
            this.wcfService.Login(loggedUser, function (user) {
                dfd.resolve(user);
            }, function (err) {
                dfd.reject(err);
                console.log(err);
            });
            return dfd.promise();
        };
        UsersService.prototype.Logout = function () {
            var dfd = $.Deferred();
            this.wcfService.Logout(function () {
                dfd.resolve();
            }, function () {
                dfd.reject();
            });
            return dfd.promise();
        };
        return UsersService;
    }());
    WcfWrap.UsersService = UsersService;
})(WcfWrap || (WcfWrap = {}));
//# sourceMappingURL=UsersService.js.map