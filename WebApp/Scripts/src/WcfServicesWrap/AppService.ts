﻿namespace WcfWrap {
    export class AppService {
        private wcfService = new WcfServices.IWcfAppService();

        public GetRandomGuid(): JQueryPromise<string> {
            let dfd: JQueryDeferred<string> = $.Deferred();

            this.wcfService.GetRandomGuid((guid: string) => {
                dfd.resolve(guid);
            }, (err: WcfServices.WcfErrorContract) => {
                App.Utils.checkIfUnautorized(err);
                dfd.reject(err);
            });

            return dfd.promise();
        }
    }
}