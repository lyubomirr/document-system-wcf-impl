﻿namespace Vms {
    export class ApplicationVm implements ActionVm {
        private currentActionVm: KnockoutObservable<ActionVm> = ko.observable();

        private isFcViewVisible: KnockoutObservable<boolean> = ko.observable(true);
        private modalTitle: KnockoutObservable<String> = ko.observable("");
        private modalText: KnockoutObservable<String> = ko.observable("");
        private modalCb: CallbackFunction = () => { };

        constructor() {
            this.openFcList();
        }

        public getTemplateName(): string {
            return "app-template";
        }

        private openFileCabinet(fileCabinet: DataVms.FileCabinetViewModel): void {
            App.Service.wcf.documentsService.GetCount(fileCabinet.Guid)
                .then((count: number) => {
                    App.Config.currentDataCount(count);
                    this.currentActionVm(new DocumentsVm(DocumentsMode.FileCabinet, fileCabinet.toModel()));
                });
        }

        private openArchive(): void {
            App.Service.wcf.documentsService.GetArchiveCount()
                .then((count: number) => {
                    App.Config.currentDataCount(count);
                    this.currentActionVm(new DocumentsVm(DocumentsMode.Archive));
                });
        }

        private openTrash(): void {
            App.Service.wcf.documentsService.GetDeletedCount()
                .then((count: number) => {
                    App.Config.currentDataCount(count);
                    this.currentActionVm(new DocumentsVm(DocumentsMode.Trash));
                });
        }

        private openFcList(): void {
            App.Service.wcf.fileCabinetsService.GetCount()
                .then((count: number) => {
                    App.Config.currentDataCount(count);
                    this.currentActionVm(new FileCabinetsVm());
                });
        }

        private deleteFileCabinetFromDocView(caller: DataVms.FileCabinetViewModel): void {
            App.Service.wcf.fileCabinetsService.Delete(caller.Guid)
                .then(() => {
                    this.openFcList();                    
                });
        }

        private openConfirmModal(titleKey: string, textKey: string, cb: CallbackFunction, name?: string): void {
            App.Utils.showModal("#confirm-modal");
            this.modalTitle(App.Service.locResources[titleKey]);
            if (name) {
                this.modalText(App.Service.locResources[textKey] + ' "' + name + '" ?');
            } else {
                this.modalText(App.Service.locResources[textKey]);
            }
            this.modalCb = cb;
        }     
    }
}