﻿namespace Vms {
    export class RootVm {
        private currentActionVm: KnockoutObservable<ActionVm> = ko.observable();

        constructor() {
            App.Service.wcf.usersService.CheckAuthorization()
                .then((user: Models.User) => {
                    App.Service.currentUser(user);
                    this.initApp();
                }, () => {
                    this.currentActionVm(new LoginVm());
                });

            Arbiter.subscribe("logged", () => {
                this.initApp();
            })
        }

        public initApp(): void {
            $.when(App.Service.init())
                .then(() => {
                    this.currentActionVm(new ApplicationVm());
                });
        }

        private logout(): void {
            App.Service.wcf.usersService.Logout()
                .then(() => {
                    App.Service.currentUser(null);
                    this.currentActionVm(new LoginVm());
                });
        }
    }
}