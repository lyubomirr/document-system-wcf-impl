﻿namespace Vms {
    export class FileCabinetsVm implements ActionVm {
        //General
        private currentUser: KnockoutObservable<DataVms.UserViewModel> = ko.observable(new DataVms.UserViewModel(App.Service.currentUser()));
        private fcModalData: KnockoutObservable<DataVms.FileCabinetViewModel> = ko.observable(new DataVms.FileCabinetViewModel());
        public fileCabinets: KnockoutObservableArray<DataVms.FileCabinetViewModel> = ko.observableArray(new Array<DataVms.FileCabinetViewModel>());
        public isEdit: KnockoutObservable<boolean> = ko.observable(false);

        //Authorization
        private shouldBeActive: KnockoutComputed<boolean>;
        private isAdmin: KnockoutComputed<boolean>;

        //Pagination
        public maxPageNumber: KnockoutComputed<number>;

        //Query
        private searchQuery: KnockoutObservable<DataVms.SearchQueryViewModel>
            = ko.observable(new DataVms.SearchQueryViewModel());


        constructor() {           
            this.shouldBeActive = ko.computed(() => {
                return this.currentUser().Role.Name !== UserRoles.Guest;
            });
            this.isAdmin = ko.computed(() => {
                return this.currentUser().Role.Name === UserRoles.Admin;
            });

            this.maxPageNumber = ko.computed(() => {
                return Math.ceil(App.Config.currentDataCount() / this.searchQuery().ElementsPerPage());
            });

            this.searchQuery().ElementsPerPage.subscribe(() => {
                if (this.searchQuery().PageNumber() > this.maxPageNumber()) {
                    this.searchQuery().PageNumber(this.maxPageNumber());
                }
            });

            ko.computed(() => {
                this.searchQuery().ElementsPerPage();
                this.searchQuery().IsAscending();
                this.searchQuery().PageNumber();
                this.searchQuery().SearchValue();
                this.searchQuery().SortProperty();

                if (!ko.computedContext.isInitial()) {
                    App.Service.wcf.fileCabinetsService.GetCount(this.searchQuery().toModel())
                        .then((count: number) => {
                            App.Config.currentDataCount(count);
                            this.loadFileCabinets();
                        });
                }

            });

            this.loadFileCabinets();
        }

        public getTemplateName(): string {
            return "file-cabinets-template";
        }

        public loadFileCabinets(): void {
            App.Service.wcf.fileCabinetsService.GetFileCabinets(this.searchQuery().toModel())
                .then((fcs: Models.FileCabinet[]) => {
                    let parsed: DataVms.FileCabinetViewModel[] = [];
                    for (let fc of fcs) {
                        parsed.push(new DataVms.FileCabinetViewModel(fc));
                    }
                    this.fileCabinets(parsed);
                })
        }

        private addFileCabinet(): void {
            if (!this.isFcModelValid()) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
                return;
            }

            App.Service.wcf.fileCabinetsService.Add(this.fcModalData().toModel())
                .then((returnedFc: Models.FileCabinet) => {
                    if (this.fileCabinets().length == this.searchQuery().ElementsPerPage()) {
                        this.fileCabinets.pop();
                    }
                    this.fileCabinets.unshift(new DataVms.FileCabinetViewModel(returnedFc));
                    this.fcModalData(new DataVms.FileCabinetViewModel());
                    App.Utils.hideModal("#file-cabinet-modal");

                });
        }

        private deleteFileCabinet(caller: DataVms.FileCabinetViewModel): void {
            App.Service.wcf.fileCabinetsService.Delete(caller.Guid)
                .then(() => {
                    this.fileCabinets.remove(caller);
                });
        }

        private updateFileCabinet(): void {
            if (!this.isFcModelValid()) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
                return;
            }
            App.Service.wcf.fileCabinetsService.Update(this.fcModalData().toModel())
               .then(() => {
                   for (let el of this.fileCabinets()) {
                       if (el.Guid == this.fcModalData().Guid) {
                           this.fileCabinets.replace(el, this.fcModalData());
                           App.Utils.hideModal("#file-cabinet-modal");
                           break;
                       }
                   };
               });
        }

        private getNextPage(): void {
            if (this.searchQuery().PageNumber() < this.maxPageNumber()) {
                this.searchQuery().PageNumber(this.searchQuery().PageNumber() + 1);
            }
        }

        private getPrevPage(): void {
            if (this.searchQuery().PageNumber() > 1) {
                this.searchQuery().PageNumber(this.searchQuery().PageNumber() - 1);
            }
        }

        private isFcModelValid(): boolean {
            return App.Utils.isInputValid(this.fcModalData().Name());
        }

        private sortByProperty(propName: string) {
            if (this.searchQuery().SortProperty() != propName) {
                this.searchQuery().SortProperty(propName);
                this.searchQuery().IsAscending(true);
            } else {
                this.searchQuery().IsAscending(!this.searchQuery().IsAscending());
            }
        }

        private setModalData(fc?: DataVms.FileCabinetViewModel) {
            if (fc) {
                let clone: DataVms.FileCabinetViewModel = new DataVms.FileCabinetViewModel(fc.toModel());
                this.fcModalData(clone);
            } else {
                this.fcModalData(new DataVms.FileCabinetViewModel());
            }
        }

        private openFcModal(fc?: DataVms.FileCabinetViewModel) {
            if (fc) {
                this.isEdit(true);
            } else {
                this.isEdit(false);
            }
            this.setModalData(fc);
        }

    }
}