var Vms;
(function (Vms) {
    var DocumentsVm = /** @class */ (function () {
        function DocumentsVm(mode, fileCabinet) {
            var _this = this;
            //General
            this.currentUser = ko.observable(new DataVms.UserViewModel(App.Service.currentUser()));
            this.documents = ko.observableArray(new Array());
            this.currentSelection = [];
            this.currentFileCabinet = new Models.FileCabinet();
            this.sectionTitle = ko.observable("");
            //Modal data
            this.docModalData = ko.observable(new DataVms.DocumentViewModel());
            this.addDocIsCopy = ko.observable(false);
            this.addDocSelectValue = ko.observable("");
            this.archiveName = ko.observable("");
            this.exportData = ko.observableArray([]);
            this.isArchive = ko.observable(false);
            this.isTrash = ko.observable(false);
            //File view settings
            this.currentFileExt = ko.observable("");
            this.shouldHavePreview = ko.observable(false);
            this.isDoctypeActive = ko.observable(true);
            //Query
            this.searchQuery = ko.observable(new DataVms.SearchQueryViewModel());
            this.modalDocTypeName = ko.computed(function () {
                return App.Service.dataMapper.getDocumentTypeName(_this.addDocSelectValue());
            });
            this.isAllChecked = ko.computed(function () {
                for (var _i = 0, _a = _this.documents(); _i < _a.length; _i++) {
                    var doc = _a[_i];
                    if (!doc.isChecked()) {
                        return false;
                    }
                }
                return true;
            });
            this.isAdmin = ko.computed(function () {
                return _this.currentUser().Role.Name === UserRoles.Admin;
            });
            this.isGuest = ko.computed(function () {
                return _this.currentUser().Role.Name === UserRoles.Guest;
            });
            this.maxPageNumber = ko.computed(function () {
                return Math.ceil(App.Config.currentDataCount() / _this.searchQuery().ElementsPerPage());
            });
            this.shouldBeActive = ko.computed(function () {
                if (_this.isArchive() || _this.isTrash()) {
                    return false;
                }
                else {
                    return !_this.isGuest();
                }
            });
            this.searchQuery().ElementsPerPage.subscribe(function () {
                if (_this.searchQuery().PageNumber() > _this.maxPageNumber()) {
                    _this.searchQuery().PageNumber(_this.maxPageNumber());
                }
            });
            ko.computed(function () {
                _this.searchQuery().ElementsPerPage();
                _this.searchQuery().IsAscending();
                _this.searchQuery().PageNumber();
                _this.searchQuery().SearchValue();
                _this.searchQuery().SortProperty();
                _this.searchQuery().Type();
                if (!ko.computedContext.isInitial()) {
                    _this.getDocumentCount().then(function (count) {
                        App.Config.currentDataCount(count);
                        _this.loadDocs();
                    });
                }
            });
            switch (mode) {
                case DocumentsMode.FileCabinet: {
                    this.setFileCabinet(fileCabinet);
                    break;
                }
                case DocumentsMode.Archive: {
                    this.setArchive();
                    break;
                }
                case DocumentsMode.Trash: {
                    this.setTrash();
                    break;
                }
            }
        }
        DocumentsVm.prototype.getTemplateName = function () {
            return "documents-template";
        };
        DocumentsVm.prototype.setFileCabinet = function (fileCabinet) {
            this.currentFileCabinet = fileCabinet;
            this.sectionTitle(this.currentFileCabinet.Name);
            this.isArchive(false);
            this.isTrash(false);
            this.loadDocs();
        };
        DocumentsVm.prototype.setArchive = function () {
            this.sectionTitle(App.Service.locResources['ArchiveTitle']);
            this.isArchive(true);
            this.isTrash(false);
            this.loadDocs();
        };
        DocumentsVm.prototype.setTrash = function () {
            this.sectionTitle(App.Service.locResources['TrashTitle']);
            this.isArchive(false);
            this.isTrash(true);
            this.loadDocs();
        };
        DocumentsVm.prototype.addDocument = function (doc) {
            var _this = this;
            App.Service.wcf.documentsService.Add(doc)
                .then(function (received) {
                if (_this.documents().length == _this.searchQuery().ElementsPerPage()) {
                    _this.documents.pop();
                }
                _this.documents.unshift(new DataVms.DocumentViewModel(received));
                App.Utils.hideProgressBar();
                App.Utils.hideModal("#add-document-modal");
            })
                .catch(function (err) {
                console.log(err);
                App.Utils.hideProgressBar();
                App.Utils.alertInvalidDataError(".form-error-alert", "Error!");
            });
        };
        DocumentsVm.prototype.deleteDocuments = function (guids) {
            var _this = this;
            App.Service.wcf.documentsService.Delete(guids)
                .then(function () {
                _this.documents(_this.documents().filter(function (doc) {
                    return guids.indexOf(doc.Guid) === -1;
                }));
                _this.currentSelection = _this.currentSelection.filter(function (guid) {
                    return guids.indexOf(guid) === -1;
                });
                _this.refreshDocumentsAfterRemoval();
            });
        };
        DocumentsVm.prototype.archivateDocument = function (doc) {
            var _this = this;
            App.Service.wcf.documentsService.Archivate(doc.Guid)
                .then(function () {
                _this.documents.remove(doc);
                _this.refreshDocumentsAfterRemoval();
            });
        };
        DocumentsVm.prototype.restoreDeletedDocuments = function (guids) {
            var _this = this;
            App.Service.wcf.documentsService.RestoreDeleted(guids)
                .then(function () {
                _this.documents(_this.documents().filter(function (doc) {
                    return guids.indexOf(doc.Guid) === -1;
                }));
                _this.refreshDocumentsAfterRemoval();
            });
        };
        DocumentsVm.prototype.restoreAllDeletedDocuments = function () {
            var _this = this;
            App.Service.wcf.documentsService.RestoreAllDeleted()
                .then(function () {
                _this.documents([]);
            });
        };
        DocumentsVm.prototype.restoreArchivedDocuments = function (guids) {
            var _this = this;
            App.Service.wcf.documentsService.RestoreArchivated(guids)
                .then(function () {
                _this.documents(_this.documents().filter(function (doc) {
                    return guids.indexOf(doc.Guid) === -1;
                }));
                _this.refreshDocumentsAfterRemoval();
            });
        };
        DocumentsVm.prototype.restoreAllArchivedDocuments = function () {
            var _this = this;
            App.Service.wcf.documentsService.RestoreAllArchivated()
                .then(function () {
                _this.documents([]);
            });
        };
        DocumentsVm.prototype.updateDocument = function () {
            var _this = this;
            if (!this.isDocModelValid()) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
                return;
            }
            App.Service.wcf.documentsService.Update(this.docModalData().getModel())
                .then(function () {
                console.log(_this.docModalData());
                for (var _i = 0, _a = _this.documents(); _i < _a.length; _i++) {
                    var single = _a[_i];
                    if (single.Guid == _this.docModalData().Guid) {
                        _this.documents.replace(single, _this.docModalData());
                        App.Utils.hideModal("#view-document-modal");
                        break;
                    }
                }
            });
        };
        DocumentsVm.prototype.copyDocument = function () {
            var _this = this;
            if (!this.isDocModelValid()) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
                return;
            }
            App.Service.wcf.documentsService.Copy(this.docModalData().getModel())
                .then(function (newDocument) {
                if (_this.documents().length == _this.searchQuery().ElementsPerPage()) {
                    _this.documents.pop();
                }
                _this.documents.unshift(new DataVms.DocumentViewModel(newDocument));
                App.Utils.hideModal("#add-document-modal");
            });
        };
        DocumentsVm.prototype.archivateCurrentFileCabinet = function () {
            var _this = this;
            App.Service.wcf.fileCabinetsService.Archivate(this.currentFileCabinet.Guid)
                .then(function () {
                _this.documents([]);
            });
        };
        DocumentsVm.prototype.setArchiveName = function (name) {
            var date = new Date;
            var displayedName = name.replace(/\s/g, '') + "." + date.toISOString().substring(0, 10);
            this.archiveName(displayedName);
        };
        DocumentsVm.prototype.setExportModalData = function (guids) {
            this.shouldHavePreview(false);
            this.exportData(guids);
            if (guids.length === 1) {
                var docs = this.documents().filter(function (el) {
                    return el.Guid == guids[0];
                });
                this.setArchiveName(docs[0].Name());
                this.setDocModalData(docs[0]);
            }
            else {
                this.setArchiveName(this.currentFileCabinet.Name);
            }
            App.Utils.showModal("#export-document-modal");
        };
        DocumentsVm.prototype.openFileView = function (doc) {
            var ext = App.Utils.getFileExtension(doc.FilePath());
            this.addDocIsCopy(false);
            this.isDoctypeActive(false);
            this.shouldHavePreview(App.Config.supportedPreviewExtensions.indexOf(ext) !== -1);
            this.currentFileExt(ext);
            this.setDocModalData(doc);
        };
        DocumentsVm.prototype.setDocModalData = function (doc) {
            var clone = new DataVms.DocumentViewModel(doc.getModel());
            if (this.addDocIsCopy()) {
                clone.Name(clone.Name() + " - copy");
            }
            this.docModalData(clone);
            this.addDocSelectValue(clone.Type().Guid);
        };
        DocumentsVm.prototype.openSaveNewDocumentForm = function (isCopy, doc) {
            this.addDocIsCopy(isCopy);
            this.isDoctypeActive(!isCopy);
            this.shouldHavePreview(false);
            if (isCopy) {
                this.setDocModalData(doc);
            }
            else {
                this.setDocModalData(new DataVms.DocumentViewModel());
            }
        };
        DocumentsVm.prototype.modifySelection = function (doc) {
            if (doc.isChecked()) {
                var index = this.currentSelection.indexOf(doc.Guid);
                this.currentSelection.splice(index, 1);
            }
            else {
                this.currentSelection.push(doc.Guid);
            }
            doc.isChecked(!doc.isChecked());
            return true;
        };
        DocumentsVm.prototype.importDocuments = function () {
            var _this = this;
            var file = $("#importFile")[0].files[0];
            var ext = App.Utils.getFileExtension(file.name);
            if (ext != ".zip") {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources["ZipFormatError"]);
                return;
            }
            App.Service.wcf.appService.GetRandomGuid()
                .then(function (guid) {
                App.Service.connManager
                    .uploadFileByChunks(App.Service.endpoints.uploadChunks + guid, file)
                    .then(function (fileName) {
                    var importInfo = new Models.UploadedFileInfo(guid, _this.currentFileCabinet.Guid, fileName);
                    App.Service.wcf.documentsService.CompleteImport(importInfo)
                        .then(function (importedDocs) {
                        for (var _i = 0, importedDocs_1 = importedDocs; _i < importedDocs_1.length; _i++) {
                            var doc = importedDocs_1[_i];
                            _this.documents.unshift(new DataVms.DocumentViewModel(doc));
                        }
                        App.Utils.hideProgressBar();
                        App.Utils.hideModal("#import-documents-modal");
                    })
                        .catch(function (err) {
                        App.Utils.hideProgressBar();
                        App.Utils.alertInvalidDataError(".form-error-alert", err.get_message());
                    });
                }).catch(function () {
                    App.Utils.hideProgressBar();
                    App.Utils.alertInvalidDataError(".form-error-alert", "Error!");
                });
            });
        };
        DocumentsVm.prototype.exportDocuments = function () {
            if (App.Utils.isInputValid(this.archiveName())) {
                var url = App.Service.endpoints.exportDocuments + "?filename=" + this.archiveName() + "&guids=" + this.exportData().join();
                App.Utils.hideModal("#export-document-modal");
                window.location.href = url;
            }
            else {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
            }
        };
        DocumentsVm.prototype.exportCurrentFileCabinet = function () {
            if (App.Utils.isInputValid(this.archiveName())) {
                var url = App.Service.endpoints.exportFileCabinet + "?filename=" + this.archiveName() + "&guid=" + this.currentFileCabinet.Guid;
                App.Utils.hideModal("#export-document-modal");
                window.location.href = url;
            }
            else {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
            }
        };
        DocumentsVm.prototype.toggleAll = function () {
            var _this = this;
            if (this.isAllChecked()) {
                this.documents().map(function (el) {
                    el.isChecked(false);
                    var index = _this.currentSelection.indexOf(el.Guid);
                    _this.currentSelection.splice(index, 1);
                });
            }
            else {
                this.documents().map(function (doc) {
                    if (_this.currentSelection.indexOf(doc.Guid) === -1) {
                        doc.isChecked(true);
                        _this.currentSelection.push(doc.Guid);
                    }
                });
            }
            return true;
        };
        DocumentsVm.prototype.loadDocs = function () {
            var _this = this;
            this.getDocs()
                .then(function (docs) {
                var parsed = [];
                for (var _i = 0, docs_1 = docs; _i < docs_1.length; _i++) {
                    var doc = docs_1[_i];
                    var docVm = new DataVms.DocumentViewModel(doc);
                    if (_this.currentSelection.indexOf(docVm.Guid) !== -1) {
                        docVm.isChecked(true);
                    }
                    parsed.push(docVm);
                }
                _this.documents(parsed);
            });
        };
        DocumentsVm.prototype.getDocs = function () {
            if (this.isTrash()) {
                return App.Service.wcf.documentsService.GetDeleted(this.searchQuery().toModel());
            }
            else if (this.isArchive()) {
                return App.Service.wcf.documentsService.GetArchive(this.searchQuery().toModel());
            }
            else {
                return App.Service.wcf.documentsService
                    .GetDocuments(this.currentFileCabinet.Guid, this.searchQuery().toModel());
            }
        };
        DocumentsVm.prototype.saveFile = function () {
            var _this = this;
            if (!this.isDocModelValid()) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
                return;
            }
            App.Service.wcf.appService.GetRandomGuid()
                .then(function (guid) {
                App.Service.connManager
                    .uploadFileByChunks(App.Service.endpoints.uploadChunks + guid, $("#File")[0].files[0])
                    .then(function (fileName) {
                    var fileInfo = new Models.UploadedFileInfo(guid, _this.currentFileCabinet.Guid, fileName);
                    App.Service.wcf.documentsService.CompleteUpload(fileInfo)
                        .then(function (filePath) {
                        var model = _this.docModalData().getModel();
                        model.TypeId = _this.addDocSelectValue();
                        model.FileCabinetId = _this.currentFileCabinet.Guid;
                        model.FilePath = filePath;
                        model.Guid = guid;
                        _this.addDocument(model);
                    })
                        .catch(function () {
                        App.Utils.hideProgressBar();
                        App.Utils.alertInvalidDataError(".form-error-alert", "Error!");
                    });
                }).catch(function () {
                    App.Utils.hideProgressBar();
                    App.Utils.alertInvalidDataError(".form-error-alert", "Error!");
                });
            });
        };
        DocumentsVm.prototype.getNextPage = function () {
            if (this.searchQuery().PageNumber() < this.maxPageNumber()) {
                this.searchQuery().PageNumber(this.searchQuery().PageNumber() + 1);
            }
        };
        DocumentsVm.prototype.getPrevPage = function () {
            if (this.searchQuery().PageNumber() > 1) {
                this.searchQuery().PageNumber(this.searchQuery().PageNumber() - 1);
            }
        };
        DocumentsVm.prototype.isDocModelValid = function () {
            return App.Utils.isInputValid(this.docModalData().Name()) && App.Utils.isInputValid(this.addDocSelectValue());
        };
        DocumentsVm.prototype.sortByProperty = function (propName) {
            if (this.searchQuery().SortProperty() != propName) {
                this.searchQuery().SortProperty(propName);
                this.searchQuery().IsAscending(true);
            }
            else {
                this.searchQuery().IsAscending(!this.searchQuery().IsAscending());
            }
        };
        DocumentsVm.prototype.getDocumentCount = function () {
            if (this.isTrash()) {
                return App.Service.wcf.documentsService.GetDeletedCount(this.searchQuery().toModel());
            }
            else if (this.isArchive()) {
                return App.Service.wcf.documentsService.GetArchiveCount(this.searchQuery().toModel());
            }
            else {
                return App.Service.wcf.documentsService.GetCount(this.currentFileCabinet.Guid, this.searchQuery().toModel());
            }
        };
        DocumentsVm.prototype.refreshDocumentsAfterRemoval = function () {
            var _this = this;
            this.getDocumentCount().then(function (count) {
                App.Config.currentDataCount(count);
                if (_this.searchQuery().PageNumber() > _this.maxPageNumber()) {
                    _this.searchQuery().PageNumber(_this.maxPageNumber());
                    return;
                }
                _this.loadDocs();
            });
        };
        return DocumentsVm;
    }());
    Vms.DocumentsVm = DocumentsVm;
})(Vms || (Vms = {}));
//# sourceMappingURL=DocumentsVm.js.map