var Vms;
(function (Vms) {
    var FileCabinetsVm = /** @class */ (function () {
        function FileCabinetsVm() {
            var _this = this;
            //General
            this.currentUser = ko.observable(new DataVms.UserViewModel(App.Service.currentUser()));
            this.fcModalData = ko.observable(new DataVms.FileCabinetViewModel());
            this.fileCabinets = ko.observableArray(new Array());
            this.isEdit = ko.observable(false);
            //Query
            this.searchQuery = ko.observable(new DataVms.SearchQueryViewModel());
            this.shouldBeActive = ko.computed(function () {
                return _this.currentUser().Role.Name !== UserRoles.Guest;
            });
            this.isAdmin = ko.computed(function () {
                return _this.currentUser().Role.Name === UserRoles.Admin;
            });
            this.maxPageNumber = ko.computed(function () {
                return Math.ceil(App.Config.currentDataCount() / _this.searchQuery().ElementsPerPage());
            });
            this.searchQuery().ElementsPerPage.subscribe(function () {
                if (_this.searchQuery().PageNumber() > _this.maxPageNumber()) {
                    _this.searchQuery().PageNumber(_this.maxPageNumber());
                }
            });
            ko.computed(function () {
                _this.searchQuery().ElementsPerPage();
                _this.searchQuery().IsAscending();
                _this.searchQuery().PageNumber();
                _this.searchQuery().SearchValue();
                _this.searchQuery().SortProperty();
                if (!ko.computedContext.isInitial()) {
                    App.Service.wcf.fileCabinetsService.GetCount(_this.searchQuery().toModel())
                        .then(function (count) {
                        App.Config.currentDataCount(count);
                        _this.loadFileCabinets();
                    });
                }
            });
            this.loadFileCabinets();
        }
        FileCabinetsVm.prototype.getTemplateName = function () {
            return "file-cabinets-template";
        };
        FileCabinetsVm.prototype.loadFileCabinets = function () {
            var _this = this;
            App.Service.wcf.fileCabinetsService.GetFileCabinets(this.searchQuery().toModel())
                .then(function (fcs) {
                var parsed = [];
                for (var _i = 0, fcs_1 = fcs; _i < fcs_1.length; _i++) {
                    var fc = fcs_1[_i];
                    parsed.push(new DataVms.FileCabinetViewModel(fc));
                }
                _this.fileCabinets(parsed);
            });
        };
        FileCabinetsVm.prototype.addFileCabinet = function () {
            var _this = this;
            if (!this.isFcModelValid()) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
                return;
            }
            App.Service.wcf.fileCabinetsService.Add(this.fcModalData().toModel())
                .then(function (returnedFc) {
                if (_this.fileCabinets().length == _this.searchQuery().ElementsPerPage()) {
                    _this.fileCabinets.pop();
                }
                _this.fileCabinets.unshift(new DataVms.FileCabinetViewModel(returnedFc));
                _this.fcModalData(new DataVms.FileCabinetViewModel());
                App.Utils.hideModal("#file-cabinet-modal");
            });
        };
        FileCabinetsVm.prototype.deleteFileCabinet = function (caller) {
            var _this = this;
            App.Service.wcf.fileCabinetsService.Delete(caller.Guid)
                .then(function () {
                _this.fileCabinets.remove(caller);
            });
        };
        FileCabinetsVm.prototype.updateFileCabinet = function () {
            var _this = this;
            if (!this.isFcModelValid()) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources['InvalidDataError']);
                return;
            }
            App.Service.wcf.fileCabinetsService.Update(this.fcModalData().toModel())
                .then(function () {
                for (var _i = 0, _a = _this.fileCabinets(); _i < _a.length; _i++) {
                    var el = _a[_i];
                    if (el.Guid == _this.fcModalData().Guid) {
                        _this.fileCabinets.replace(el, _this.fcModalData());
                        App.Utils.hideModal("#file-cabinet-modal");
                        break;
                    }
                }
                ;
            });
        };
        FileCabinetsVm.prototype.getNextPage = function () {
            if (this.searchQuery().PageNumber() < this.maxPageNumber()) {
                this.searchQuery().PageNumber(this.searchQuery().PageNumber() + 1);
            }
        };
        FileCabinetsVm.prototype.getPrevPage = function () {
            if (this.searchQuery().PageNumber() > 1) {
                this.searchQuery().PageNumber(this.searchQuery().PageNumber() - 1);
            }
        };
        FileCabinetsVm.prototype.isFcModelValid = function () {
            return App.Utils.isInputValid(this.fcModalData().Name());
        };
        FileCabinetsVm.prototype.sortByProperty = function (propName) {
            if (this.searchQuery().SortProperty() != propName) {
                this.searchQuery().SortProperty(propName);
                this.searchQuery().IsAscending(true);
            }
            else {
                this.searchQuery().IsAscending(!this.searchQuery().IsAscending());
            }
        };
        FileCabinetsVm.prototype.setModalData = function (fc) {
            if (fc) {
                var clone = new DataVms.FileCabinetViewModel(fc.toModel());
                this.fcModalData(clone);
            }
            else {
                this.fcModalData(new DataVms.FileCabinetViewModel());
            }
        };
        FileCabinetsVm.prototype.openFcModal = function (fc) {
            if (fc) {
                this.isEdit(true);
            }
            else {
                this.isEdit(false);
            }
            this.setModalData(fc);
        };
        return FileCabinetsVm;
    }());
    Vms.FileCabinetsVm = FileCabinetsVm;
})(Vms || (Vms = {}));
//# sourceMappingURL=FileCabinetsVm.js.map