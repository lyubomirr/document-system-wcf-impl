var Vms;
(function (Vms) {
    var LoginVm = /** @class */ (function () {
        function LoginVm() {
            this.loggedUser = new DataVms.UserViewModel();
        }
        LoginVm.prototype.getTemplateName = function () {
            return "login-template";
        };
        LoginVm.prototype.login = function () {
            App.Service.wcf.usersService.Login(this.loggedUser.toModel())
                .then(function (user) {
                App.Service.currentUser(user);
                Arbiter.publish("logged");
            }, function (err) {
                App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources[err.get_message()]);
            });
        };
        return LoginVm;
    }());
    Vms.LoginVm = LoginVm;
})(Vms || (Vms = {}));
//# sourceMappingURL=LoginVm.js.map