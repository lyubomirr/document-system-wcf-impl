﻿namespace Vms {
    export class LoginVm implements ActionVm {      
        private loggedUser: DataVms.UserViewModel = new DataVms.UserViewModel();

        public getTemplateName(): string {
            return "login-template";
        }

        private login(): void {
            App.Service.wcf.usersService.Login(this.loggedUser.toModel())
                .then((user: Models.User) => {
                    App.Service.currentUser(user);
                    Arbiter.publish("logged");
                }, (err: WcfServices.WcfErrorContract) => {
                    App.Utils.alertInvalidDataError(".form-error-alert", App.Service.locResources[err.get_message()]);
                });
        }
    }
}