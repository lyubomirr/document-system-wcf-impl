var Vms;
(function (Vms) {
    var ApplicationVm = /** @class */ (function () {
        function ApplicationVm() {
            this.currentActionVm = ko.observable();
            this.isFcViewVisible = ko.observable(true);
            this.modalTitle = ko.observable("");
            this.modalText = ko.observable("");
            this.modalCb = function () { };
            this.openFcList();
        }
        ApplicationVm.prototype.getTemplateName = function () {
            return "app-template";
        };
        ApplicationVm.prototype.openFileCabinet = function (fileCabinet) {
            var _this = this;
            App.Service.wcf.documentsService.GetCount(fileCabinet.Guid)
                .then(function (count) {
                App.Config.currentDataCount(count);
                _this.currentActionVm(new Vms.DocumentsVm(DocumentsMode.FileCabinet, fileCabinet.toModel()));
            });
        };
        ApplicationVm.prototype.openArchive = function () {
            var _this = this;
            App.Service.wcf.documentsService.GetArchiveCount()
                .then(function (count) {
                App.Config.currentDataCount(count);
                _this.currentActionVm(new Vms.DocumentsVm(DocumentsMode.Archive));
            });
        };
        ApplicationVm.prototype.openTrash = function () {
            var _this = this;
            App.Service.wcf.documentsService.GetDeletedCount()
                .then(function (count) {
                App.Config.currentDataCount(count);
                _this.currentActionVm(new Vms.DocumentsVm(DocumentsMode.Trash));
            });
        };
        ApplicationVm.prototype.openFcList = function () {
            var _this = this;
            App.Service.wcf.fileCabinetsService.GetCount()
                .then(function (count) {
                App.Config.currentDataCount(count);
                _this.currentActionVm(new Vms.FileCabinetsVm());
            });
        };
        ApplicationVm.prototype.deleteFileCabinetFromDocView = function (caller) {
            var _this = this;
            App.Service.wcf.fileCabinetsService.Delete(caller.Guid)
                .then(function () {
                _this.openFcList();
            });
        };
        ApplicationVm.prototype.openConfirmModal = function (titleKey, textKey, cb, name) {
            App.Utils.showModal("#confirm-modal");
            this.modalTitle(App.Service.locResources[titleKey]);
            if (name) {
                this.modalText(App.Service.locResources[textKey] + ' "' + name + '" ?');
            }
            else {
                this.modalText(App.Service.locResources[textKey]);
            }
            this.modalCb = cb;
        };
        return ApplicationVm;
    }());
    Vms.ApplicationVm = ApplicationVm;
})(Vms || (Vms = {}));
//# sourceMappingURL=ApplicationVm.js.map