var Models;
(function (Models) {
    var UploadedFileInfo = /** @class */ (function () {
        function UploadedFileInfo(Guid, FileCabinetGuid, FileName) {
            this.Guid = Guid;
            this.FileCabinetGuid = FileCabinetGuid;
            this.FileName = FileName;
        }
        return UploadedFileInfo;
    }());
    Models.UploadedFileInfo = UploadedFileInfo;
})(Models || (Models = {}));
//# sourceMappingURL=UploadedFileInfo.js.map