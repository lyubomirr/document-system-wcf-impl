var Models;
(function (Models) {
    var FileUpload = /** @class */ (function () {
        function FileUpload(Guid, FileName, FileStream) {
            this.Guid = Guid;
            this.FileName = FileName;
            this.FileStream = FileStream;
        }
        return FileUpload;
    }());
    Models.FileUpload = FileUpload;
})(Models || (Models = {}));
//# sourceMappingURL=FileUpload.js.map