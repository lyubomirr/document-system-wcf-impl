var Models;
(function (Models) {
    var Role = /** @class */ (function () {
        function Role(Guid, Name) {
            if (Guid === void 0) { Guid = null; }
            if (Name === void 0) { Name = ""; }
            this.Guid = Guid;
            this.Name = Name;
        }
        return Role;
    }());
    Models.Role = Role;
})(Models || (Models = {}));
//# sourceMappingURL=Role.js.map