﻿namespace Models {
    export class SearchQuery {
        constructor(
            public SearchValue: string = null,
            public PageNumber: number = null,
            public ElementsPerPage: number = null,
            public SortProperty: string = null,
            public IsAscending: boolean = null,
            public Type: string = null 
        ) { }
    }
}