var Models;
(function (Models) {
    var User = /** @class */ (function () {
        function User(Guid, Username, Password, Salt, RoleId) {
            if (Guid === void 0) { Guid = null; }
            if (Username === void 0) { Username = ""; }
            if (Password === void 0) { Password = ""; }
            if (Salt === void 0) { Salt = ""; }
            if (RoleId === void 0) { RoleId = null; }
            this.Guid = Guid;
            this.Username = Username;
            this.Password = Password;
            this.Salt = Salt;
            this.RoleId = RoleId;
        }
        return User;
    }());
    Models.User = User;
})(Models || (Models = {}));
//# sourceMappingURL=User.js.map