var Models;
(function (Models) {
    var FileCabinet = /** @class */ (function () {
        function FileCabinet(Guid, Name, Deleted) {
            if (Guid === void 0) { Guid = ""; }
            if (Name === void 0) { Name = ""; }
            if (Deleted === void 0) { Deleted = false; }
            this.Guid = Guid;
            this.Name = Name;
            this.Deleted = Deleted;
        }
        return FileCabinet;
    }());
    Models.FileCabinet = FileCabinet;
})(Models || (Models = {}));
//# sourceMappingURL=FileCabinet.js.map