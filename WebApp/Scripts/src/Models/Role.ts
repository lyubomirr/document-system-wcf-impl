﻿namespace Models {
    export class Role {
        constructor(
            public Guid: string = null,
            public Name: string = ""
        ) {}
    }
}