﻿namespace Models {
    export class UploadedFileInfo {
        constructor(
            public Guid?: string,
            public FileCabinetGuid?: string,
            public FileName?: string
        ) {}
    }
}