﻿namespace Models {
    export class User {
        constructor(
            public Guid: string = null,
            public Username: string = "",
            public Password: string = "",
            public Salt: string = "",
            public RoleId: string = null
        ) { }    
    }
}