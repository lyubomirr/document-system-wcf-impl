var Models;
(function (Models) {
    var DocumentType = /** @class */ (function () {
        function DocumentType(Guid, Name) {
            if (Guid === void 0) { Guid = ""; }
            if (Name === void 0) { Name = ""; }
            this.Guid = Guid;
            this.Name = Name;
        }
        return DocumentType;
    }());
    Models.DocumentType = DocumentType;
})(Models || (Models = {}));
//# sourceMappingURL=DocumentType.js.map