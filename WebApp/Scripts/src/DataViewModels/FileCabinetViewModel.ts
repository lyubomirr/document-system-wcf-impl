﻿namespace DataVms {
    export class FileCabinetViewModel {
        public Guid: string = null;
        public Name: KnockoutObservable<string> = ko.observable("");
        public Deleted: boolean = false;

        constructor(model?: Models.FileCabinet) {
            if (model) {
                this.Guid = model.Guid;
                this.Name(model.Name);
                this.Deleted = model.Deleted;
            }
        }

        toModel(): Models.FileCabinet {
            return new Models.FileCabinet(
                this.Guid,
                this.Name(),
                this.Deleted
            );
        } 

    }
}