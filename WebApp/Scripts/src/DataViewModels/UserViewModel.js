var DataVms;
(function (DataVms) {
    var UserViewModel = /** @class */ (function () {
        function UserViewModel(model) {
            this.model = new Models.User();
            this.Guid = null;
            this.Username = "";
            this.Password = "";
            this.Role = new Models.Role();
            if (model) {
                this.model = model;
                this.Guid = model.Guid;
                this.Username = model.Username;
                this.Role = App.Service.dataMapper.getRole(model.RoleId);
            }
        }
        UserViewModel.prototype.toModel = function () {
            this.model.Username = this.Username;
            this.model.Password = this.Password;
            return this.model;
        };
        return UserViewModel;
    }());
    DataVms.UserViewModel = UserViewModel;
})(DataVms || (DataVms = {}));
//# sourceMappingURL=UserViewModel.js.map