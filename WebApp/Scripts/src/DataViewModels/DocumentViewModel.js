var DataVms;
(function (DataVms) {
    var DocumentViewModel = /** @class */ (function () {
        function DocumentViewModel(model) {
            var _this = this;
            this.model = new Models.Document();
            this.Guid = null;
            this.Name = ko.observable("");
            this.Description = ko.observable("");
            this.Type = ko.observable(new Models.DocumentType());
            this.StoreDate = ko.observable(new Date());
            this.Amount = ko.observable();
            this.Vat = ko.observable();
            this.TotalAmount = ko.computed(function () {
                if (_this.Amount() && _this.Vat()) {
                    return _this.Amount() * (1 + _this.Vat() / 100);
                }
                return null;
            });
            this.FilePath = ko.observable("");
            this.Subject = ko.observable("");
            this.Owner = ko.observable("");
            this.RegDocNumber = ko.observable("");
            this.Contact = ko.observable("");
            this.Company = ko.observable("");
            this.isChecked = ko.observable(false);
            if (model) {
                this.model = model;
                this.Guid = model.Guid;
                this.Name(model.Name);
                this.Description(model.Description);
                this.Type(App.Service.dataMapper.getDocumentType(model.TypeId));
                this.StoreDate(model.StoreDate);
                this.Amount(model.Amount);
                this.Vat(model.Vat);
                this.FilePath(model.FilePath);
                this.Subject(model.Subject);
                this.Owner(model.Owner.Username);
                this.RegDocNumber(model.RegDocNumber);
                this.Contact(model.Contact);
                this.Company(model.Company);
            }
        }
        DocumentViewModel.prototype.getModel = function () {
            this.model.Name = this.Name();
            this.model.Description = this.Description();
            this.model.Amount = this.Amount();
            this.model.Vat = this.Vat();
            this.model.TotalAmount = this.TotalAmount();
            this.model.Subject = this.Subject();
            this.model.RegDocNumber = this.RegDocNumber();
            this.model.Contact = this.Contact();
            this.model.Company = this.Company();
            return this.model;
        };
        return DocumentViewModel;
    }());
    DataVms.DocumentViewModel = DocumentViewModel;
})(DataVms || (DataVms = {}));
//# sourceMappingURL=DocumentViewModel.js.map