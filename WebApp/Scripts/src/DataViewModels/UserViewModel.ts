﻿namespace DataVms {
    export class UserViewModel {
        private model: Models.User = new Models.User();
        public Guid: string = null;
        public Username: string = "";
        public Password: string = "";
        public Role: Models.Role = new Models.Role();

        constructor(model?: Models.User) {
            if (model) {
                this.model = model;
                this.Guid = model.Guid;
                this.Username = model.Username;
                this.Role = App.Service.dataMapper.getRole(model.RoleId);
            }
        }

        public toModel(): Models.User {
            this.model.Username = this.Username;
            this.model.Password = this.Password; 

            return this.model;
        }
    }
}