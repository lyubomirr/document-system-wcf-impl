var DataVms;
(function (DataVms) {
    var SearchQueryViewModel = /** @class */ (function () {
        function SearchQueryViewModel(SearchValue, PageNumber, ElementsPerPage, IsAscending, SortProperty, Type) {
            if (SearchValue === void 0) { SearchValue = ko.observable(""); }
            if (PageNumber === void 0) { PageNumber = ko.observable(1); }
            if (ElementsPerPage === void 0) { ElementsPerPage = ko.observable(5); }
            if (IsAscending === void 0) { IsAscending = ko.observable(true); }
            if (SortProperty === void 0) { SortProperty = ko.observable("Name"); }
            if (Type === void 0) { Type = ko.observable(); }
            var _this = this;
            this.SearchValue = SearchValue;
            this.PageNumber = PageNumber;
            this.ElementsPerPage = ElementsPerPage;
            this.IsAscending = IsAscending;
            this.SortProperty = SortProperty;
            this.Type = Type;
            this.SearchValue.subscribe(function () {
                _this.PageNumber(1);
            });
            this.Type.subscribe(function () {
                _this.PageNumber(1);
            });
        }
        SearchQueryViewModel.prototype.toModel = function () {
            var model = new Models.SearchQuery(this.SearchValue(), this.PageNumber(), this.ElementsPerPage(), this.SortProperty(), this.IsAscending(), this.Type());
            return model;
        };
        return SearchQueryViewModel;
    }());
    DataVms.SearchQueryViewModel = SearchQueryViewModel;
})(DataVms || (DataVms = {}));
//# sourceMappingURL=SearchQueryViewModel.js.map