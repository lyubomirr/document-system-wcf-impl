$(document).ready(function () {
    App = new Global.Application();
    App.Service = new Services.ApplicationService();
    App.Utils = new Services.Utils();
    App.Config = new Services.Config();
    App.Service.getResources()
        .then(function () {
        var rootVm = new Vms.RootVm();
        ko.applyBindings(rootVm, $("#root")[0]);
    });
});
//# sourceMappingURL=AppInit.js.map