﻿using Models.DbEntities;
using System;
using System.Collections.Generic;

namespace Interfaces.Services
{
    public interface IUsersService
    {
        IList<User> GetUsers(string username = null);
        User GetUserById(Guid guid);
        User Login(User loggingUser);
    }
}
