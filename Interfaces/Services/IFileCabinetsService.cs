﻿using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;

namespace Interfaces.Services
{
    public interface IFileCabinetsService
    {
        IList<FileCabinet> GetFileCabinets(SearchQuery searchQuery = null);
        int GetCount(SearchQuery searchQuery = null); 
        FileCabinet Add(FileCabinet fileCabinet);
        void Delete(Guid guid);
        void Archivate(Guid guid);
        void Update(FileCabinet updated);
    }
}
