﻿using Models.DbEntities;
using System.Collections.Generic;

namespace Interfaces.Services
{
    public interface IDocumentTypesService
    {
        IList<DocumentType> GetDocumentTypes();
    }
}
