﻿using System.Collections.Generic;

namespace Interfaces.Data
{
    public interface IDataAdapter
    {
        bool AddData(string tableName, List<Dictionary<string, object>> data);
        List<Dictionary<string, object>> GetData(string tableName, string sqlQuery);
        bool UpdateData(Dictionary<string, object> newData, string tableName, string sqlQuery);
        bool DeleteData(string tableName, string sqlQuery);
        int GetTableRowCount(string tableName, string sqlQuery);
    }

}
