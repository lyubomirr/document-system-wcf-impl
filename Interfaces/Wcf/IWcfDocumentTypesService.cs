﻿using Models.DbEntities;
using System.Collections.Generic;
using System.ServiceModel;

namespace Interfaces.Wcf
{
    [ServiceContract(Namespace = "WcfServices")]
    public interface IWcfDocumentTypesService
    {
        [OperationContract]
        IList<DocumentType> GetDocumentTypes();
    }
}
