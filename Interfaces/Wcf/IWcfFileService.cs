﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace Interfaces.Wcf
{
    [ServiceContract]
    public interface IWcfFileService
    {
        [OperationContract]
        [WebGet(UriTemplate = "/exportdocuments?filename={filename}&guids={guids}")]
        Stream ExportDocuments(string filename, string guids);

        [OperationContract]
        [WebGet(UriTemplate = "/getfile?guid={guid}&isFileView={isFileView}")]
        Stream GetFile(string guid, bool isFileView = false);

        [OperationContract]
        [WebGet(UriTemplate = "/exportfilecabinet?filename={filename}&guid={guid}")]
        Stream ExportFileCabinet(string filename, string guid);

    }
}
