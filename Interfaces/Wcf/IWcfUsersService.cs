﻿using Models.DbEntities;
using System.ServiceModel;

namespace Interfaces.Wcf
{
    [ServiceContract(Namespace = "WcfServices")]
    public interface IWcfUsersService
    {
        [OperationContract]
        User CheckAuthorization();
        [OperationContract]
        User Login(User loggedUser);
        [OperationContract]
        void Logout();
    }
}
