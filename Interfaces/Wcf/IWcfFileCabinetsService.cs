﻿using Models.DbEntities;
using Models.Dtos;
using System.Collections.Generic;
using System.ServiceModel;

namespace Interfaces.Wcf
{
    [ServiceContract(Namespace = "WcfServices")]
    public interface IWcfFileCabinetsService
    {
        [OperationContract]
        IList<FileCabinet> GetFileCabinets(SearchQuery searchQuery);

        [OperationContract]
        int GetCount(SearchQuery searchQuery);

        [OperationContract]
        FileCabinet Add(FileCabinet fileCabinet);

        [OperationContract]
        void Delete(string guid);

        [OperationContract]
        void Archivate(string guid);

        [OperationContract]
        void Update(FileCabinet updated);

    }
}
