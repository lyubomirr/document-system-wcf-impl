﻿using System;
using System.Collections.Generic;
using System.ServiceModel;


namespace Interfaces.Wcf
{
    [ServiceContract(Namespace = "WcfServices")]
    public interface IWcfAppService
    {
        [OperationContract]
        Guid GetRandomGuid();
    }
}
