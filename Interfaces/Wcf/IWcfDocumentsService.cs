﻿using Models.DbEntities;
using Models.Dtos;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;

namespace Interfaces.Wcf
{
    [ServiceContract(Namespace = "WcfServices")]
    public interface IWcfDocumentsService
    {
        [OperationContract]
        IList<Document> GetDocuments(string fcGuid, SearchQuery searchQuery);

        [OperationContract]
        IList<Document> GetDeleted(SearchQuery searchQuery);

        [OperationContract]
        IList<Document> GetArchive(SearchQuery searchQuery);

        [OperationContract]
        int GetCount(string fcGuid, SearchQuery searchQuery);

        [OperationContract]
        int GetDeletedCount(SearchQuery searchQuery);

        [OperationContract]
        int GetArchiveCount(SearchQuery searchQuery);

        [OperationContract]
        void Delete(IEnumerable<string> guids);

        [OperationContract]
        void Update(Document doc);

        [OperationContract]
        void Archivate(string guid);

        [OperationContract]
        void RestoreArchivated(IEnumerable<string> guids);

        [OperationContract]
        void RestoreAllArchivated();

        [OperationContract]
        void RestoreDeleted(IEnumerable<string> guids);

        [OperationContract]
        void RestoreAllDeleted();

        [OperationContract]
        Document Copy(Document newDocument);

        [OperationContract]
        string CompleteUpload(UploadedFileInfo fileInfo);

        [OperationContract]
        Document Add(Document doc);

        [OperationContract]
        IList<Document> CompleteImport(UploadedFileInfo importInfo);

        [OperationContract]
        Stream ExportDocuments(IEnumerable<string> guids);

        [OperationContract]
        Stream ExportDocumentsFromFileCabinet(string fcGuid);

        [OperationContract]
        FileStream GetFileStream(string guid);
    }
}
