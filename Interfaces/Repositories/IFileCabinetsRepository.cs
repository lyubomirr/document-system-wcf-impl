﻿using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;

namespace Interfaces.Repositories
{
    public interface IFileCabinetsRepository
    {
        IList<FileCabinet> GetFileCabinets(SearchQuery searchQuery = null);
        FileCabinet GetFileCabinetById(Guid guid);
        FileCabinet UpdateFileCabinet(FileCabinet updatedFileCabinet);
        bool AddFileCabinet(FileCabinet fileCabinet);
        int GetCount(SearchQuery searchQuery = null);
    }
}
