﻿using Models.DbEntities;
using System;
using System.Collections.Generic;


namespace Interfaces.Repositories
{
    public interface IUsersRepository
    {
        IList<User> GetUsers(string username = null);
        User GetUserById(Guid guid);
    }
}
