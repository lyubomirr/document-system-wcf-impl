﻿using JWT;
using JWT.Algorithms;
using JWT.Serializers;

namespace WcfServicesLayer.Authorize
{
    internal static class Jwt
    {
        private static string secret = "m3d48SyVPx9TEyqT4ZAtx7tVKIc5F0OELY3iCaHK4Go";
        
        public static string CreateToken(JwtPayload payload)
        {
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var token = encoder.Encode(payload, secret);
            return token;
        }

        public static JwtPayload DecodeToken(string token)
        {
            IJsonSerializer serializer = new JsonNetSerializer();
            IDateTimeProvider provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

            var payload = decoder.DecodeToObject<JwtPayload>(token);
            return payload;
        }       
    }
}