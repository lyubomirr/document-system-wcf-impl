﻿using JWT;
using System.ServiceModel.Web;
using System.Web;

namespace WcfServicesLayer.Authorize
{
    public static class JwtAuth
    {
        public static bool AuthorizeUser(HttpCookie jwt)
        {
            try
            {
                if (jwt == null)
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                    throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
                }

                Jwt.DecodeToken(jwt.Value);
                return true;
            }
            catch (TokenExpiredException)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
            }
            catch (SignatureVerificationException)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                throw new WebFaultException(System.Net.HttpStatusCode.Unauthorized);
            }
        }

        public static bool AuthorizeNotGuest(HttpCookie jwt)
        {
            var payload = Jwt.DecodeToken(jwt.Value);
            return payload.Role == "Admin" || payload.Role == "Regular";
        }
    }
}