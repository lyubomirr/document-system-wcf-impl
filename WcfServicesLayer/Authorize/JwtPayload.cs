﻿using System;

namespace WcfServicesLayer.Authorize
{
    public class JwtPayload
    {
        public Guid Guid { get; set; }
        public string Role { get; set; }
        public long exp { get; set; }
    }
}