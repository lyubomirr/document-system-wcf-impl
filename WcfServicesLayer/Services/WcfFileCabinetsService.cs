﻿using System;
using System.Collections.Generic;
using System.Web;
using Interfaces.Services;
using Interfaces.Wcf;
using Models.DbEntities;
using Models.Dtos;
using WcfServicesLayer.Authorize;

namespace WcfServicesLayer.Services
{
    public class WcfFileCabinetsService : IWcfFileCabinetsService
    {
        private IFileCabinetsService _fileCabinetService;

        public WcfFileCabinetsService(IFileCabinetsService fileCabinetsService)
        {
            _fileCabinetService = fileCabinetsService;
        }

        public IList<FileCabinet> GetFileCabinets(SearchQuery searchQuery)
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));

            var fcs = _fileCabinetService.GetFileCabinets(searchQuery);
            return fcs;
        }

        public int GetCount(SearchQuery searchQuery)
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));

            int count = _fileCabinetService.GetCount(searchQuery);
            return count;
        }

        public FileCabinet Add(FileCabinet fileCabinet)
        {
            var jwtToken = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtToken);
            JwtAuth.AuthorizeNotGuest(jwtToken);

            _fileCabinetService.Add(fileCabinet);
            return fileCabinet;
        }

        public void Delete(string guid)
        {
            var jwtToken = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtToken);
            JwtAuth.AuthorizeNotGuest(jwtToken);

            _fileCabinetService.Delete(Guid.Parse(guid));
        }

        public void Archivate(string guid)
        {
            var jwtToken = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtToken);
            JwtAuth.AuthorizeNotGuest(jwtToken);

            _fileCabinetService.Archivate(Guid.Parse(guid));
        }

        public void Update(FileCabinet updated)
        {
            var jwtToken = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtToken);
            JwtAuth.AuthorizeNotGuest(jwtToken);

            _fileCabinetService.Update(updated);
        }
    }
}
