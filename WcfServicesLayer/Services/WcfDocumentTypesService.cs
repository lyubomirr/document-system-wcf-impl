﻿using Interfaces.Data;
using Interfaces.Services;
using Interfaces.Wcf;
using Models.DbEntities;
using System.Collections.Generic;
using System.Web;
using WcfServicesLayer.Authorize;

namespace WcfServicesLayer.Services
{
    public class WcfDocumentTypesService : IWcfDocumentTypesService
    {
        private IDocumentTypesService _documentTypesService;

        public WcfDocumentTypesService(IDocumentTypesService documentTypesService)
        {
            _documentTypesService = documentTypesService;
        }

        public IList<DocumentType> GetDocumentTypes()
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));

            var docTypes = _documentTypesService.GetDocumentTypes();
            return docTypes;
        }
    }
}
