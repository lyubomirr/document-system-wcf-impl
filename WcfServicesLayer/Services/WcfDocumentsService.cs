﻿using Interfaces.Services;
using Interfaces.Wcf;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Web;
using WcfServicesLayer.Authorize;

namespace WcfServicesLayer.Services
{ 
    public class WcfDocumentsService : IWcfDocumentsService
    {
        private IDocumentsService _documentsService;
        public WcfDocumentsService(IDocumentsService documentsService)
        {
            _documentsService = documentsService;
        }

        public IList<Document> GetDocuments(string fcGuid, SearchQuery searchQuery)
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));

            var docs = _documentsService.GetDocuments(Guid.Parse(fcGuid), searchQuery);
            return docs;
        }

        public IList<Document> GetDeleted(SearchQuery searchQuery)
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));

            var docs = _documentsService.GetDeleted(searchQuery);
            return docs;
        }

        public IList<Document> GetArchive(SearchQuery searchQuery)
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));

            var docs = _documentsService.GetArchive(searchQuery);
            return docs;
        }

        public int GetCount(string fcGuid, SearchQuery searchQuery)
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));

            int count = _documentsService.GetCount(Guid.Parse(fcGuid), searchQuery);
            return count;
        }

        public int GetDeletedCount(SearchQuery searchQuery)
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));

            int count = _documentsService.GetDeletedCount(searchQuery);
            return count;
        }

        public int GetArchiveCount(SearchQuery searchQuery)
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));

            int count = _documentsService.GetArchiveCount(searchQuery);
            return count;
        }

        public void Delete(IEnumerable<string> guids)
        {
            var parsedGuids = guids.Select(guid => Guid.Parse(guid));
            _documentsService.Delete(parsedGuids);
        }

        public void Update(Document doc)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            var payload = Jwt.DecodeToken(jwtCookie.Value);
            _documentsService.Update(doc, payload.Guid);
        }

        public void Archivate(string guid)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            _documentsService.Archivate(Guid.Parse(guid));
        }

        public void RestoreArchivated(IEnumerable<string> guids)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            var parsedGuids = guids.Select(guid => Guid.Parse(guid));
            _documentsService.RestoreArchivated(parsedGuids);      
        }

        public void RestoreAllArchivated()
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            _documentsService.RestoreAllArchivated();
        }

        public void RestoreDeleted(IEnumerable<string> guids)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            var parsedGuids = guids.Select(guid => Guid.Parse(guid));
            _documentsService.RestoreDeleted(parsedGuids);
        }

        public void RestoreAllDeleted()
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            _documentsService.RestoreAllDeleted();
        }

        public Document Copy(Document newDocument)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            var copied = _documentsService.Copy(newDocument);
            return copied;
        }

        public string CompleteUpload(UploadedFileInfo fileInfo)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            var newfilePath = _documentsService.CompleteUpload(fileInfo);
            return newfilePath;
        }

        public Document Add(Document doc)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            var payload = Jwt.DecodeToken(HttpContext.Current.Request.Cookies.Get("jwt").Value);
            var addedDoc = _documentsService.Add(doc, payload.Guid);
            return addedDoc;
        }

        public IList<Document> CompleteImport(UploadedFileInfo importInfo)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            try
            {
                var importedDocs = _documentsService.CompleteImport(importInfo);
                return importedDocs;
            }
            catch(InvalidOperationException ex)
            {
                throw new FaultException(ex.Message);
            }
        }

        public Stream ExportDocuments(IEnumerable<string> guids)
        {
            var parsedGuids = guids.Select(guid => Guid.Parse(guid));
            var exported = _documentsService.ExportDocuments(parsedGuids);
            return exported;
        }

        public Stream ExportDocumentsFromFileCabinet(string fcGuid)
        {
            var exported = _documentsService.ExportDocumentsFromFileCabinet(Guid.Parse(fcGuid));
            return exported;
        }  

        public FileStream GetFileStream(string guid)
        {
            var fileStream = _documentsService.GetFileStream(Guid.Parse(guid));
            return fileStream;
        }
    }
}
