﻿using Interfaces.Data;
using Interfaces.Services;
using Interfaces.Wcf;
using Models.DbEntities;
using System;
using System.Security.Authentication;
using System.ServiceModel;
using System.Web;
using WcfServicesLayer.Authorize;

namespace WcfServicesLayer.Services
{
    public class WcfUsersService : IWcfUsersService
    {
        private IUsersService _usersService;
        private IRolesService _rolesService;
        private IDataQuery _dq;

        public WcfUsersService(IUsersService usersService, IRolesService rolesService, IDataQuery dq)
        {
            _usersService = usersService;
            _rolesService = rolesService;
            _dq = dq;
        }

        public User CheckAuthorization()
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));
            var payload = Jwt.DecodeToken(HttpContext.Current.Request.Cookies.Get("jwt").Value);
            return _usersService.GetUserById(payload.Guid);
        }

        public User Login(User loggingUser)
        {
           try
            {
                var user = _usersService.Login(loggingUser);
                SaveCredentials(user, HttpContext.Current.Response);
                return user;
            }
            catch(AuthenticationException ex)
            {
                throw new FaultException(ex.Message);
            }
        }

        public void Logout()
        {
            HttpCookie cookie = new HttpCookie("jwt");
            cookie.Expires = DateTime.Now.AddDays(-1d);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        private void SaveCredentials(User user, HttpResponse response)
        {
            var role = _rolesService.GetRoleById((Guid)user.RoleId);
            JwtPayload payload = new JwtPayload();

            payload.Guid = (Guid)user.Guid;
            payload.Role = role.Name;
            payload.exp = ((DateTimeOffset)(DateTime.Now.AddHours(8))).ToUnixTimeSeconds();

            var token = Jwt.CreateToken(payload);
            HttpCookie cookie = new HttpCookie("jwt", token);
            cookie.Expires = DateTime.Now.AddHours(8);
            response.Cookies.Add(cookie);
        }
    }
}
