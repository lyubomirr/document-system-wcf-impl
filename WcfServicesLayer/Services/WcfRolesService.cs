﻿using Interfaces.Services;
using Interfaces.Wcf;
using Models;
using Models.DbEntities;
using System.Collections.Generic;
using System.Web;
using WcfServicesLayer.Authorize;

namespace WcfServicesLayer.Services
{
    public class WcfRolesService : IWcfRolesService
    {
        private IRolesService _rolesService;

        public WcfRolesService(IRolesService rolesService)
        {
            _rolesService = rolesService;
        }

        public IList<Role> GetRoles()
        {
            JwtAuth.AuthorizeUser(HttpContext.Current.Request.Cookies.Get("jwt"));

            var roles = _rolesService.GetRoles();
            return roles;
        }
    }
}
