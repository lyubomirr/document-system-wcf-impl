﻿using Interfaces.Wcf;
using System;
using System.Web;
using WcfServicesLayer.Authorize;

namespace WcfServicesLayer.Services
{
    public class WcfAppService : IWcfAppService
    {
        public Guid GetRandomGuid()
        {
            var jwt = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwt);
            JwtAuth.AuthorizeNotGuest(jwt);

            return Guid.NewGuid();
        }
    }
}
