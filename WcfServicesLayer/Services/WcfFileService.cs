﻿using Interfaces.Services;
using Interfaces.Wcf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using WcfServicesLayer.Authorize;

namespace WcfServicesLayer.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FileService" in both code and config file together.
    public class WcfFileService : IWcfFileService
    { 
        private IDocumentsService _documentsService;
        public WcfFileService(IDocumentsService documentsService)
        {
            _documentsService = documentsService;
        }


        public Stream ExportDocuments(string filename, string guids)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            var parsedGuids = guids.Split(',').Select(guid => Guid.Parse(guid));
            var file = _documentsService.ExportDocuments(parsedGuids);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/zip";
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition", $"attachment; filename=\"{filename}.zip\"");
            return file;
        }

        public Stream ExportFileCabinet(string filename, string guid)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            var file = _documentsService.ExportDocumentsFromFileCabinet(Guid.Parse(guid));
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/zip";
            WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition", $"attachment; filename=\"{filename}.zip\"");
            return file;
        }

        public Stream GetFile(string guid, bool isFileView = false)
        {
            var jwtCookie = HttpContext.Current.Request.Cookies.Get("jwt");
            JwtAuth.AuthorizeUser(jwtCookie);
            JwtAuth.AuthorizeNotGuest(jwtCookie);

            var fileStream = _documentsService.GetFileStream(Guid.Parse(guid));
            var fileName = Path.GetFileName(fileStream.Name);
            WebOperationContext.Current.OutgoingResponse.ContentType = MimeTypes.GetMimeType(fileName);
            
            if (isFileView)
            {
                WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition", $"inline; filename=\"{fileName}\"");
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition", $"attachment; filename=\"{fileName}\"");
            }

            return fileStream;
        }
        
    }
}
