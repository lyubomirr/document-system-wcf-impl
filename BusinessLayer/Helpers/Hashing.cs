﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace BusinessLayer.Helpers
{
    public class Hashing
    {
        private const int _saltSize = 4;
        private HashAlgorithm _hashAlgorithm;

        public Hashing(HashAlgorithm hashAlgorithm)
        {
            _hashAlgorithm = hashAlgorithm;
        }

        public string ComputeHash(string plainText, string salt)
        {
            byte[] saltBytes = Encoding.UTF8.GetBytes(salt);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            byte[] plainTextAndSalt = new byte[plainTextBytes.Length + salt.Length];

            plainTextAndSalt = plainTextBytes.Concat(saltBytes).ToArray();          
            byte[] hashed = _hashAlgorithm.ComputeHash(plainTextAndSalt);

            string hashedString = ByteArrayToHashString(hashed);
            return hashedString;
        }

        public string GenerateSalt()
        {
            byte[] salt = new byte[_saltSize];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetNonZeroBytes(salt);
            }

            string saltString = ByteArrayToHashString(salt);
            return saltString;
        }

        public bool VerifyHash(string newPlainText, string hashValue, string salt)
        {
            return ComputeHash(newPlainText, salt) == hashValue;
        }

        private string ByteArrayToHashString(byte[] array)
        {
            StringBuilder builder = new StringBuilder();
            foreach(byte singleByte in array)
            {
                builder.Append(singleByte.ToString("x2"));
            }
            return builder.ToString();
        }
    }
}