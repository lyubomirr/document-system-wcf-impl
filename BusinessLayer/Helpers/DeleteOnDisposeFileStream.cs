﻿using System.IO;


namespace BusinessLayer.Helpers
{
    class DeleteOnDisposeFileStream : FileStream
    {
        public DeleteOnDisposeFileStream(string path, FileMode fileMode)
            :base(path, fileMode) { }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if(File.Exists(this.Name))
            {
                File.Delete(this.Name);
            }
        }  
    }
}
