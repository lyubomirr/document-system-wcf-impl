﻿using Interfaces.Repositories;
using Interfaces.Services;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace BusinessLayer.Services
{
    public class FileCabinetsService : IFileCabinetsService
    {
        private IDocumentsService _documentsService;
        private IFileCabinetsRepository _fileCabinetsRepository;
        private readonly string _uploadsDirectoryPath;

        public FileCabinetsService(IFileCabinetsRepository fileCabinetsRepository, IDocumentsService documentsService,
            string uploadsDirectoryPath)
        {
            _fileCabinetsRepository = fileCabinetsRepository;
            _documentsService = documentsService;
            _uploadsDirectoryPath = uploadsDirectoryPath;
        }

        public IList<FileCabinet> GetFileCabinets(SearchQuery searchQuery = null)
        {
            var fcs = _fileCabinetsRepository.GetFileCabinets(searchQuery);
            return fcs;
        }

        public int GetCount(SearchQuery searchQuery = null)
        {
            int count = _fileCabinetsRepository.GetCount(searchQuery);
            return count;
        }

        public FileCabinet Add(FileCabinet fileCabinet)
        {
            fileCabinet.Guid = Guid.NewGuid();
            var fcDirectoryPath = Path.Combine(_uploadsDirectoryPath, fileCabinet.Guid.ToString());
            Directory.CreateDirectory(fcDirectoryPath);

            _fileCabinetsRepository.AddFileCabinet(fileCabinet);
            return fileCabinet;
        }

        public void Delete(Guid fileCabinetGuid)
        {
            var fileCabinet = _fileCabinetsRepository.GetFileCabinetById(fileCabinetGuid);
            if(!fileCabinet.Deleted)
            {
                fileCabinet.Deleted = true;
                _fileCabinetsRepository.UpdateFileCabinet(fileCabinet);
            }

            var documentsInFc = _documentsService.GetDocuments(fileCabinetGuid);
            var documentGuids = documentsInFc.Select(doc => doc.Guid);
            _documentsService.Delete(documentGuids);
        }

        public void Archivate(Guid fileCabinetGuid)
        {
            var docsInFc = _documentsService.GetDocuments(fileCabinetGuid);
            foreach (var doc in docsInFc)
            {
                _documentsService.Archivate(doc.Guid);
            }
        }

        public void Update(FileCabinet updated)
        {
            _fileCabinetsRepository.UpdateFileCabinet(updated);
        }
    }
}
