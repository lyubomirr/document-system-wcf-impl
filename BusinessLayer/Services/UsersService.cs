﻿using Interfaces.Services;
using Interfaces.Repositories;
using Models.DbEntities;
using BusinessLayer.Helpers;
using System.Security.Cryptography;
using System.Security.Authentication;
using System.Collections.Generic;
using System;

namespace BusinessLayer.Services
{
    public class UsersService : IUsersService
    {
        private IUsersRepository _usersRepository;

        public UsersService(IUsersRepository usersRepository, IRolesRepository rolesRepository)
        {
            _usersRepository = usersRepository;
        }

        public User GetUserById(Guid guid)
        {
            var user = _usersRepository.GetUserById(guid);
            return user;
        }

        public IList<User> GetUsers(string username = null)
        {
            var users = _usersRepository.GetUsers(username);
            return users;
        }

        public User Login(User loggingUser)
        {
            Hashing hashing = new Hashing(new SHA256Managed());

            var users = GetUsers(loggingUser.Username);

            if (users.Count == 0)
            {
                throw new AuthenticationException("NoUser");
            }

            if (!hashing.VerifyHash(loggingUser.Password, users[0].Password, users[0].Salt))
            {
                throw new AuthenticationException("WrongPassword");
            }

            return users[0];
        }
    }
}
