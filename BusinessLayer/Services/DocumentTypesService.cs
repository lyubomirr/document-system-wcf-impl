﻿using Interfaces.Repositories;
using Interfaces.Services;
using Models.DbEntities;
using System.Collections.Generic;

namespace BusinessLayer.Services
{
    public class DocumentTypesService : IDocumentTypesService
    {
        private IDocumentTypesRepository _documentTypesRepository;
        public DocumentTypesService(IDocumentTypesRepository documentTypesRepository)
        {
            _documentTypesRepository = documentTypesRepository;
        }

        public IList<DocumentType> GetDocumentTypes()
        {
            var docTypes = _documentTypesRepository.GetDocumentTypes();
            return docTypes;
        }
    }
}
