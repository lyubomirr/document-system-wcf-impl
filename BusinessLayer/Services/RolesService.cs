﻿using System.Collections.Generic;
using Interfaces.Services;
using Interfaces.Repositories;
using Models.DbEntities;
using System;

namespace BusinessLayer.Services
{
    public class RolesService : IRolesService
    {
        private IRolesRepository _rolesRepository;

        public RolesService(IRolesRepository rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }

        public IList<Role> GetRoles()
        {
            var roles = _rolesRepository.GetRoles();
            return roles;
        }

        public Role GetRoleById(Guid guid)
        {
            var role = _rolesRepository.GetRoleById(guid);
            return role;
        }
    }
}
