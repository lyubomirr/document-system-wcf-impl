﻿using BusinessLayer.Helpers;
using Interfaces.Repositories;
using Interfaces.Services;
using Ionic.Zip;
using Models.DbEntities;
using Models.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace BusinessLayer.Services
{
    public class DocumentsService : IDocumentsService
    {
        private IDocumentsRepository _documentsRepository;
        private IUsersRepository _usersRepository;
        private IFileCabinetsRepository _fileCabinetsRepository;

        private readonly string _uploadsDirectoryPath;
        private readonly string _tempFilesDirectoryPath;

        public DocumentsService(IDocumentsRepository documentsRepository, IUsersRepository usersRepository,
            IFileCabinetsRepository fileCabinetsRepository, string uploadsDirectoryPath, string tempFilesDirectoryPath)
        {
            _documentsRepository = documentsRepository;
            _usersRepository = usersRepository;
            _fileCabinetsRepository = fileCabinetsRepository;
            _uploadsDirectoryPath = uploadsDirectoryPath;
            _tempFilesDirectoryPath = tempFilesDirectoryPath;
        }

        public IList<Document> GetDocuments(Guid? fcGuid = null, SearchQuery searchQuery = null)
        {
            var documentsInFc = _documentsRepository.GetDocuments(fcGuid, searchQuery);
            return documentsInFc;
        }

        public IList<Document> GetDeleted(SearchQuery searchQuery = null)
        {
            var docs = _documentsRepository.GetInactiveDocuments(InactiveState.Deleted, searchQuery);
            return docs;
        }

        public IList<Document> GetArchive(SearchQuery searchQuery = null)
        {
            var docs = _documentsRepository.GetInactiveDocuments(InactiveState.Archived, searchQuery);
            return docs;
        }

        public int GetCount(Guid? fcGuid = null, SearchQuery searchQuery = null)
        {
            int count = _documentsRepository.GetDocumentCount(fcGuid, searchQuery);
            return count;
        }

        public int GetDeletedCount(SearchQuery searchQuery = null)
        {
            int count = _documentsRepository.GetInactiveDocumentCount(InactiveState.Deleted, searchQuery);
            return count;
        }

        public int GetArchiveCount(SearchQuery searchQuery = null)
        {
            int count = _documentsRepository.GetInactiveDocumentCount(InactiveState.Archived, searchQuery);
            return count;
        }

        public void Delete(IEnumerable<Guid> guids)
        {
            foreach (var guid in guids)
            {
                var doc = _documentsRepository.GetDocumentById(guid);

                if (!doc.Archived)
                {
                    doc.Deleted = true;
                    _documentsRepository.UpdateDocument(doc);
                }
            }
        }

        public void Update(Document doc, Guid userModifiedGuid)
        {
            doc.LastModifiedDate = DateTime.Now;
            doc.LastModifiedUser = userModifiedGuid;

            SaveDocumentMetaData(doc);
            _documentsRepository.UpdateDocument(doc);
        }

        public void Archivate(Guid guid)
        {
            var doc = _documentsRepository.GetDocumentById(guid);
            if (doc == null)
            {
                return;
            }
            doc.Archived = true;
            _documentsRepository.UpdateDocument(doc);
        }

        public void RestoreArchivated(IEnumerable<Guid> guids)
        {
            foreach(var guid in guids)
            {
                var doc = _documentsRepository.GetDocumentById(guid);
                if(!doc.Deleted && doc.Archived)
                {
                    doc.Archived = false;
                    _documentsRepository.UpdateDocument(doc);
                }

            }
        }

        public void RestoreAllArchivated()
        {
            var docs = _documentsRepository.GetInactiveDocuments(InactiveState.Archived);
            foreach(var doc in docs)
            {
                doc.Archived = false;
                _documentsRepository.UpdateDocument(doc);
            }
        }

        public void RestoreDeleted(IEnumerable<Guid> guids)
        {
            foreach(var guid in guids)
            {
                var doc = _documentsRepository.GetDocumentById(guid);
                if(!doc.Archived && doc.Deleted)
                {
                    var fileCabinet = _fileCabinetsRepository.GetFileCabinetById(doc.FileCabinetId);
                    if(fileCabinet.Deleted)
                    {
                        fileCabinet.Deleted = false;
                        _fileCabinetsRepository.UpdateFileCabinet(fileCabinet);
                    }
                    doc.Deleted = false;
                    _documentsRepository.UpdateDocument(doc);
                }
            }
        }

        public void RestoreAllDeleted()
        {
            var docs = _documentsRepository.GetInactiveDocuments(InactiveState.Deleted);
            var guids = docs.Select(doc => doc.Guid);
            RestoreDeleted(guids);
        }

        public Document Copy(Document newDocument)
        {
            newDocument.CopyFromDocumentId = newDocument.Guid;
            newDocument.Guid = Guid.NewGuid();
            newDocument.StoreDate = DateTime.Now;

            string oldPath = newDocument.FilePath;
            string fileName = Path.GetFileName(oldPath);

            newDocument.FilePath = Path.Combine(
                newDocument.FileCabinetId.ToString(),
                newDocument.Guid.ToString(), 
                fileName
            );

            string directoryPath = Path.Combine(_uploadsDirectoryPath,
                newDocument.FileCabinetId.ToString(), newDocument.Guid.ToString());

            Directory.CreateDirectory(directoryPath);

            File.Copy(
                Path.Combine(_uploadsDirectoryPath, oldPath), 
                Path.Combine(_uploadsDirectoryPath, newDocument.FilePath)
            );

            SaveDocumentMetaData(newDocument);
            _documentsRepository.AddDocument(newDocument);

            return newDocument;
        }

        public void UploadChunks(string guid, string fileName, Stream chunk)
        {
            try
            {
                var directory = Directory.CreateDirectory(Path.Combine(_tempFilesDirectoryPath, guid));
                var originalFilePath = Path.Combine(directory.FullName, fileName);
                if (!File.Exists(originalFilePath))
                {
                    using (var fs = new FileStream(originalFilePath, FileMode.Create))
                    {
                        chunk.CopyTo(fs);
                    }
                    return;
                }

                AppendFile(originalFilePath, chunk);

                return;
            }
            catch (Exception)
            {
                Directory.Delete(Path.Combine(_tempFilesDirectoryPath, guid), true);
            }
        }

        public string CompleteUpload(UploadedFileInfo fileInfo)
        {

            string newFileDirectoryPath = Path.Combine(_uploadsDirectoryPath,
                fileInfo.FileCabinetGuid.ToString(), fileInfo.Guid.ToString());
               
            string newFilePath = Path.Combine(newFileDirectoryPath, fileInfo.FileName);
            string tempFilePath = Path.Combine(_tempFilesDirectoryPath, fileInfo.Guid.ToString(), fileInfo.FileName);

            Directory.CreateDirectory(newFileDirectoryPath);
            File.Copy(tempFilePath, newFilePath);
            Directory.Delete(Path.GetDirectoryName(tempFilePath), true);

            string newFileRelativePath = Path.Combine(fileInfo.FileCabinetGuid.ToString(),
                fileInfo.Guid.ToString(), fileInfo.FileName);

            return newFileRelativePath;
        }

        public Document Add(Document doc, Guid ownerGuid)
        {
            try
            {
                doc.StoreDate = DateTime.Now;
                doc.OwnerId = ownerGuid;
                doc.Owner = _usersRepository.GetUserById(ownerGuid);

                SaveDocumentMetaData(doc);
                _documentsRepository.AddDocument(doc);

                return doc;
            }
            catch (Exception)
            {
                var fileAbsPath = Path.Combine(_uploadsDirectoryPath, doc.FilePath);
                var directoryPath = Path.GetDirectoryName(fileAbsPath); 
                Directory.Delete(directoryPath, true);
                return null;
            }
        }

        public IList<Document> CompleteImport(UploadedFileInfo importInfo)
        {
            string tempFilesPath = Path.Combine(_tempFilesDirectoryPath, importInfo.Guid.ToString());
            string newFilePath = Path.Combine(tempFilesPath, importInfo.FileName);

            var importedDocs = new List<Document>();
            ZipFile zip = new ZipFile();

            try
            {
                zip = ZipFile.Read(newFilePath);
            }
            catch
            {
                Directory.Delete(Path.GetDirectoryName(newFilePath), true);
                throw new InvalidOperationException("Invalid zip file!");
            }

            var directories = GetZipDirectories(zip);
            foreach (var directory in directories)
            {
                var newDirectoryGuid = Guid.NewGuid();
                var files = zip.Where(entry => entry.FileName.StartsWith(directory)).Select(entry => entry).ToList();
                if (files.Count != 2 || !files.Any(entry => Path.GetFileName(entry.FileName) == "data.xml"))
                {
                    continue;
                }

                foreach(var entry in files)
                {
                    string onlyFileName = Path.GetFileName(entry.FileName);
                    entry.FileName = Path.Combine(newDirectoryGuid.ToString(), onlyFileName);
                    var entryPath = Path.Combine(_uploadsDirectoryPath, importInfo.FileCabinetGuid.ToString());
                    entry.Extract(entryPath);

                    if (onlyFileName == "data.xml")
                    {
                        Document deserialized;
                        try
                        {
                            deserialized = DeserializeDocument(entry.FileName, importInfo.FileCabinetGuid,
                                newDirectoryGuid);
                        }
                        catch (InvalidOperationException)
                        {
                            var documentPath = Path.Combine(entryPath, entry.FileName);
                            Directory.Delete(Path.GetDirectoryName(documentPath), true);
                            break;
                        }

                        _documentsRepository.AddDocument(deserialized);
                        importedDocs.Add(deserialized);
                    }

                }
            }

            zip.Dispose();

            Directory.Delete(Path.GetDirectoryName(newFilePath), true);
            return importedDocs;
        }

        public Stream ExportDocuments(IEnumerable<Guid> guids)
        {                       
            using (ZipFile zip = new ZipFile())
            {
                foreach (var guid in guids)
                {
                    var doc = _documentsRepository.GetDocumentById(guid);

                    var fileDirectory = Path.GetDirectoryName(doc.FilePath);
                    var dataFilePath = Path.Combine(_uploadsDirectoryPath, fileDirectory, "data.xml");
            
                    if (File.Exists(dataFilePath))
                    {
                        zip.AddFile(dataFilePath, doc.Guid.ToString());
                    }
                    if (File.Exists(Path.Combine(_uploadsDirectoryPath, doc.FilePath)))
                    {
                        zip.AddFile(Path.Combine(_uploadsDirectoryPath, doc.FilePath), doc.Guid.ToString());
                    }
                }

                var stream = new DeleteOnDisposeFileStream(
                    Path.Combine(_tempFilesDirectoryPath, Path.GetRandomFileName()),
                    FileMode.Create);

                zip.Save(stream);
            
                stream.Seek(0, SeekOrigin.Begin);
                return stream;
            }
        }

        public Stream ExportDocumentsFromFileCabinet(Guid fcGuid)
        {
            var docsInFc = _documentsRepository.GetDocuments(fcGuid);
            if (docsInFc.Count == 0)
            {
                return null;
            }

            var guids = docsInFc.Select(doc => doc.Guid);
            return ExportDocuments(guids);
        }

        public FileStream GetFileStream(Guid guid)
        {
            var doc = _documentsRepository.GetDocumentById(guid);
            if (doc == null)
            {
                return null;
            }
            var fullPath = Path.Combine(_uploadsDirectoryPath, doc.FilePath);
            var fs = new FileStream(fullPath, FileMode.Open);
            return fs;
        }

        private List<string> GetZipDirectories(ZipFile zip)
        {
            List<string> directories = new List<string>();
            foreach (var entry in zip.Entries)
            {
                directories.Add(Path.GetDirectoryName(entry.FileName));
            }
            return directories.Select(dir => dir).Distinct().ToList();
        }

        private void SaveDocumentMetaData(Document doc)
        {
            var fileDirectory = Path.GetDirectoryName(doc.FilePath);
            var metaDataFilePath = Path.Combine(_uploadsDirectoryPath, fileDirectory, "data.xml");

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Document));
            using (FileStream file = new FileStream(metaDataFilePath, FileMode.Create))
            {
                xmlSerializer.Serialize(file, doc);
            }
        }

        private Document DeserializeDocument(string entryName, Guid fileCabinetGuid, 
            Guid newGuid)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Document));

            var xmlFilePath = Path.Combine(_uploadsDirectoryPath,
                fileCabinetGuid.ToString(), entryName);

            Document deserialized = new Document();
            using (FileStream fs = new FileStream(xmlFilePath, FileMode.Open))
            {
                deserialized = (Document)serializer.Deserialize(fs);
            }

            string fileName = Path.GetFileName(deserialized.FilePath);

            deserialized.Guid = newGuid;
            deserialized.FileCabinetId = fileCabinetGuid;
            deserialized.FilePath = Path.Combine(fileCabinetGuid.ToString(),
                deserialized.Guid.ToString(), fileName);
            deserialized.StoreDate = DateTime.Now;
            deserialized.Owner = _usersRepository.GetUserById((Guid)deserialized.OwnerId);

            return deserialized;
        }

        private void AppendFile(string masterFilePath, Stream partial)
        {
            using (FileStream master = new FileStream(masterFilePath, FileMode.Append))
            {   
                    byte[] partialContent = new byte[partial.Length];
                    partial.Read(partialContent, 0, partialContent.Length);
                    master.Write(partialContent, 0, partialContent.Length);
            }
        }
    }
}
