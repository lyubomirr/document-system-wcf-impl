﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace DatabaseConnectionLayer
{
    public abstract class DatabaseDataAdapter : Interfaces.Data.IDataAdapter
    {
        protected string _connectionString;

        public DatabaseDataAdapter(string connectionString)
        {
            _connectionString = connectionString;
        }

        public bool AddData(string tableName, List<Dictionary<string, object>> data)
        {
            int rowsAffected = 0;

            using (var conn = GetDbConnection())
            {
                conn.Open();

                foreach (Dictionary<string, object> row in data)
                {
                    int count = row.Count;

                    List<string> keys = new List<string>();
                    List<string> values = new List<string>();

                    foreach (var field in row)
                    {
                        if (field.Value != null)
                        {
                            keys.Add(field.Key);
                            values.Add(field.Value.ToString());
                        }
                    }

                    string keysJoined = "(" + String.Join(",", keys) + ")";
                    string valuesJoined = "(" + String.Join(",", values) + ")";

                    string cmdText = $"INSERT INTO {tableName} {keysJoined} VALUES {valuesJoined};";

                    using (var command = GetDbCommand(cmdText, conn))
                    {
                        rowsAffected += command.ExecuteNonQuery();
                    }
                }
            }

            return rowsAffected > 0;
        }

        public bool DeleteData(string tableName, string sqlQuery)
        {
            if (String.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            string cmdTxt = $"DELETE FROM {tableName}";
            cmdTxt += sqlQuery;
            using (var conn = GetDbConnection())
            {
                conn.Open();

                using (var command = GetDbCommand(cmdTxt, conn))
                {
                    return command.ExecuteNonQuery() > 0;
                }
            }
        }

        public List<Dictionary<string, object>> GetData(string tableName, string sqlQuery)
        {
            if (String.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            DataTable table = new DataTable(tableName);
            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
            string cmdText = $"SELECT * FROM {tableName}";
            cmdText += sqlQuery;


            using (var conn = GetDbConnection())
            {
                conn.Open();
                using (var command = GetDbCommand(cmdText, conn))
                {
                    var reader = command.ExecuteReader();
                    table.Load(reader);
                }
            }

            foreach (DataRow row in table.Rows)
            {
                Dictionary<string, object> currentRow = new Dictionary<string, object>();

                foreach (DataColumn column in table.Columns)
                {
                    currentRow.Add(column.ToString(), row[column]);
                }

                data.Add(currentRow);
            }

            return data;
        }

        public bool UpdateData(Dictionary<string, object> newData, string tableName, string sqlQuery)
        {
            if (String.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            int rowsAffected = 0;
            using (var conn = GetDbConnection())
            {
                conn.Open();

                string cmdTxt = $"UPDATE {tableName} SET ";
                string vals = "";

                foreach (KeyValuePair<string, object> item in newData)
                {
                    if (item.Value != null)
                    {
                        vals += (item.Key + "=" + item.Value + ", ");
                    }
                }

                vals = vals.Remove(vals.Length - 2);
                cmdTxt += vals;
                cmdTxt += sqlQuery;

                using (var command = GetDbCommand(cmdTxt, conn))
                {
                    rowsAffected = command.ExecuteNonQuery();
                }
            }

            return rowsAffected > 0;
        }

        public int GetTableRowCount(string tableName, string sqlQuery)
        {
            if (String.IsNullOrEmpty(tableName))
            {
                throw new ArgumentNullException("tableName");
            }

            DataTable data = new DataTable(tableName);
            string cmdText = $"SELECT COUNT(*) FROM {tableName}";
            cmdText += sqlQuery;

            using (var conn = GetDbConnection())
            {
                conn.Open();
                using (var command = GetDbCommand(cmdText, conn))
                {
                    /* In mysql we can only cast the count result to long but same is not valid 
                     * in sql sever, so in order to work both ways we cast it to string and parse it. */

                    string count = command.ExecuteScalar().ToString();              
                    return int.Parse(count);
                }
            }
        }

        protected abstract DbConnection GetDbConnection();
        protected abstract DbCommand GetDbCommand(string cmdTxt, DbConnection connection);
    }
}
