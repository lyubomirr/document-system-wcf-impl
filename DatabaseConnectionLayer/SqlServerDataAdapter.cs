﻿using Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DatabaseConnectionLayer
{
    public class SqlServerDataAdapter : DatabaseDataAdapter
    {
        public SqlServerDataAdapter(string connectionString)
            : base(connectionString) { }

        protected override DbCommand GetDbCommand(string cmdTxt, DbConnection connection)
        {
            if (connection is SqlConnection sqlConnection)
            {
                return new SqlCommand(cmdTxt, sqlConnection);
            }
            throw new ArgumentException("Wrong db connection!");
        }

        protected override DbConnection GetDbConnection()
        {
            return new SqlConnection(_connectionString);
        }
    }
}
