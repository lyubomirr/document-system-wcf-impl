﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class SqlServerDataQuery : DataQuery
    {
        protected override string GetPaginationQuery()
        {
            return $" OFFSET(({_pageNumber} - 1) * {_elemPerPage}) ROWS FETCH NEXT {_elemPerPage} ROWS ONLY";
        }
    }
}
