﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class MySqlDataQuery : DataQuery
    {
        protected override string GetPaginationQuery()
        {
            return $" LIMIT {_elemPerPage} OFFSET {(_pageNumber - 1) * _elemPerPage}";
        }
    }
}
