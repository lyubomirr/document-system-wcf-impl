﻿using Configuration;
using Interfaces.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessLayer
{
    public abstract class DataQuery : IDataQuery
    {
        private string _sortField;
        private string _joinForeignKey;
        private string _joinPrimaryKey;
        private bool _isAsc;
        private Dictionary<string, object> _queries;
        private Dictionary<string, object> _searches;
        private TableMapping _tableMapping;
        private TableMapping _joinedTableMapping;

        protected int? _pageNumber;
        protected int _elemPerPage;

        public DataQuery()
        {
            _queries = new Dictionary<string, object>();
            _searches = new Dictionary<string, object>();
        }

        protected abstract string GetPaginationQuery();

        private string GetField(string propertyName)
        {
            Pair propertyPair = _tableMapping.Pairs.FirstOrDefault(pair => pair.Model == propertyName);
            if (propertyPair == null)
            {
                throw new ArgumentException($"No field corresponds to property {propertyName} in table {_tableMapping.TableName}!");
            }
            return propertyPair.DataSource;
        }

        private void Clean()
        {
            _queries = new Dictionary<string, object>();
            _searches = new Dictionary<string, object>();
            _sortField = null;
            _isAsc = true;
            _pageNumber = null;
            _joinForeignKey = null;
            _joinPrimaryKey = null;
            _joinedTableMapping = null;
        }

        public IDataQuery Init(string tableName)
        {
            _tableMapping = TableMapper.GetTableMapping(tableName);

            if (_tableMapping == null)
            {
                throw new ArgumentException("Invalid table name!");
            }

            Clean();

            return this;
        }

        public IDataQuery Init(TableMapping tableMapping)
        {
            _tableMapping = tableMapping;

            if (_tableMapping == null)
            {
                throw new ArgumentException("Invalid table name!");
            }

            Clean();

            return this;
        }

        public IDataQuery AddQuery(string property, string value)
        {
            if(!String.IsNullOrEmpty(property) && !String.IsNullOrEmpty(value))
            {
                string dbField = GetField(property);
                _queries.Add(dbField, $"'{value}'");
            }
            return this;
        }

        public IDataQuery AddQuery(string property, Guid? value)
        {
            if(value != null)
            {
                return AddQuery(property, value.ToString());
            }
            return this;
        }

        public IDataQuery AddQuery(string property, object value)
        {
            if (!String.IsNullOrEmpty(property) && value != null)
            {
                string dbField = GetField(property);
                _queries.Add(dbField, value);
            }
            return this;
        }

        public IDataQuery SortBy(string sortProperty, bool? isAsc = true)
        {
            if(!String.IsNullOrEmpty(sortProperty))
            {
                string dbSortField = GetField(sortProperty);
                _sortField = dbSortField;
                _isAsc = isAsc ?? true;
            }
            return this;
        }

        public IDataQuery GetPage(int? pageNumber, int? elemPerPage = null)
        {
            if(pageNumber != null)
            {
                _pageNumber = pageNumber;
                _elemPerPage = elemPerPage ?? 5;
            }
            return this;
        }

        public IDataQuery SearchBy(string property, string value)
        {    
            if (!String.IsNullOrEmpty(property) && !String.IsNullOrEmpty(value))
            {
                string dbField = GetField(property);
                _searches.Add(dbField, value.Trim());
            }
            return this;
        }

        public IDataQuery SearchBy(string[] properties, string value)
        {
            if(properties == null)
            {
                return this;
            }

            foreach(string property in properties)
            {
                if(!String.IsNullOrEmpty(property) && !String.IsNullOrEmpty(value))
                {
                    string dbField = GetField(property);
                    _searches.Add(dbField, value.Trim());
                }
            }
            return this;
        }

        public IDataQuery Join(string joinTable, string foreignProperty, string primaryProperty)
        {
            _joinedTableMapping = TableMapper.GetTableMapping(joinTable);
            if (_joinedTableMapping == null)
            {
                throw new ArgumentException($"No such table in DB as {joinTable}");
            }

            var primaryPropertyPair = _joinedTableMapping.Pairs.FirstOrDefault(pair => pair.Model == primaryProperty);
            if (primaryPropertyPair == null)
            {
                throw new ArgumentException($"No field corresponds to property {primaryProperty} in table {joinTable}!");
            }

            _joinPrimaryKey = primaryPropertyPair.DataSource;
            _joinForeignKey = GetField(foreignProperty);

            return this;
        }

        public IDataQuery Join(TableMapping joinedMapping, string foreignProperty, string primaryProperty)
        {
            _joinedTableMapping = joinedMapping;
            if (_joinedTableMapping == null)
            {
                throw new ArgumentException($"No such table in DB as {_joinedTableMapping.TableName}");
            }

            var primaryPropertyPair = _joinedTableMapping.Pairs.FirstOrDefault(pair => pair.Model == primaryProperty);
            if (primaryPropertyPair == null)
            {
                throw new ArgumentException($"No field corresponds to property {primaryProperty} in table {_joinedTableMapping.TableName}!");
            }

            _joinPrimaryKey = primaryPropertyPair.DataSource;
            _joinForeignKey = GetField(foreignProperty);

            return this;
        }

        public string ToSql()
        {
            string sql = "";

            if (_joinedTableMapping != null 
                && !String.IsNullOrEmpty(_joinedTableMapping.TableName)
                && !String.IsNullOrEmpty(_joinForeignKey)
                && !String.IsNullOrEmpty(_joinPrimaryKey)
                )
            {
                sql += $" LEFT JOIN {_joinedTableMapping.TableName} ON {_tableMapping.TableName}.{_joinForeignKey} = {_joinedTableMapping.TableName}.{_joinPrimaryKey}";
            }
            if (_queries.Count > 0)
            {
                sql += " WHERE ";
                foreach (var query in _queries)
                {
                    sql += ($"{query.Key}={query.Value} AND ");
                }

                if(_searches.Count == 0)
                {
                    //We remove the last AND statement.
                    sql = sql.Substring(0, sql.Length - 5);
                }
            }
            
            if(_searches.Count > 0)
            {
                if(_queries.Count == 0)
                {
                    sql += " WHERE ";
                }
                sql += "(";
                foreach(var search in _searches)
                {
                    sql += ($"{search.Key} LIKE '%{search.Value}%' OR ");
                }

                //Remove last OR .
                sql = sql.Substring(0, sql.Length - 4);
                sql += ")";
            }

            if(!String.IsNullOrEmpty(_sortField))
            {
                sql += $" ORDER BY {_sortField}";
                sql = _isAsc ? sql + " ASC" : sql + " DESC";

                if (_pageNumber != null)
                {
                    sql += GetPaginationQuery();
                }

            }
            
            return sql;
        }

        public TableMapping GetMapping()
        {
            return _tableMapping;
        }

        public TableMapping GetJoinedMapping()
        {
            return _joinedTableMapping;
        }
    }
}
