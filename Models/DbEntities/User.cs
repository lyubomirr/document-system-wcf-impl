﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.DbEntities
{
    public class User
    {
        [Required]
        public Guid? Guid { get; set; }

        [Required(ErrorMessage = "Enter username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Enter password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string Salt { get; set; }

        [Required]
        public Guid? RoleId { get; set; }
    }
}