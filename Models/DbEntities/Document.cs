﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Models.DbEntities
{
    [Serializable]
    [DataContract]
    public class Document
    {
        [XmlIgnore]
        [DataMember]
        public Guid Guid { get; set; }

        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public Guid TypeId { get; set; }
        [DataMember]
        public DateTime? StoreDate { get; set; }
        [DataMember]
        public decimal? Amount { get; set; }
        [DataMember]
        public int? Vat { get; set; }
        [DataMember]
        public decimal? TotalAmount { get; set; }
        [DataMember]
        public Guid? LastModifiedUser { get; set; }
        [DataMember]
        public DateTime? LastModifiedDate { get; set; }
        [DataMember]
        public bool Archived { get; set; }
        [DataMember]
        public bool Deleted { get; set; }
        [DataMember]
        [XmlIgnore]
        public Guid FileCabinetId { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public Guid? CopyFromDocumentId { get; set; }
        [DataMember]
        public string Subject { get; set; }
        [DataMember]
        public Guid? OwnerId { get; set; }
        [DataMember]
        [XmlIgnore]
        public User Owner { get; set; }
        [DataMember]
        public string RegDocNumber { get; set; }
        [DataMember]
        public string Contact { get; set; }
        [DataMember]
        public string Company { get; set; }
    }
}