﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.DbEntities
{
    public class FileCabinet
    {
        [Required]
        public Guid? Guid { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [Required]
        public bool Deleted { get; set; }
    }
}