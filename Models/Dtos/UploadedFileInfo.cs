﻿using System;
using System.Runtime.Serialization;

namespace Models.Dtos
{
    [DataContract]
    public class UploadedFileInfo
    {
        [DataMember]
        public Guid Guid { get; set; }
        [DataMember]
        public Guid FileCabinetGuid { get; set; }
        [DataMember]
        public string FileName { get; set; }
    }
}