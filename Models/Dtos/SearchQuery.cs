﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Dtos
{
    public class SearchQuery
    {
        public string SearchValue { get; set; }
        public int? PageNumber { get; set; }
        public int? ElementsPerPage { get; set; }
        public string SortProperty { get; set; }
        public bool? IsAscending { get; set; }
        public string Type { get; set; }
    }
}
