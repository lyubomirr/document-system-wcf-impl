﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Configuration
{
    [Serializable]
    public class TableMappingConfig
    {
        public List<TableMapping> TableMappings { get; set; }
    }
}
