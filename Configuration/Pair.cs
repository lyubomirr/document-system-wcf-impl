﻿using System;
using System.Xml.Serialization;

namespace Configuration
{
    [Serializable]
    public class Pair
    {
        [XmlAttribute]
        public string Model { get; set; }

        [XmlAttribute]
        public string DataSource { get; set; }
    }
}
