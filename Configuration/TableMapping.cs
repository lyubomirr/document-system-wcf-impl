﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Configuration
{   
    [Serializable]
    public class TableMapping
    {
        [XmlElement("Pair")]
        public List<Pair> Pairs { get; set; }
        
        [XmlAttribute]
        public string TableName { get; set; }       
    }
}
